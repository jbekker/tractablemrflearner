#!/home/s0215494/tractablemrflearner
# Call this script with 1 parameter: the dataset
# sh gccomp.sh dataset

rm -rf /tmp/jessa
mkdir /tmp/jessa
echo "run in gccomp for dataset $1 and alpha 0.1 in background"
nohup ./Release/tractableMrfLearner2 gccomp $1 0.1 /tmp/jessa/ > /tmp/jessa/dr_gccomp_$1_d1.txt 2>&1 &
echo "run in gccomp for dataset $1 and alpha 0.001 in background"
nohup ./Release/tractableMrfLearner2 gccomp $1 0.001 /tmp/jessa/ > /tmp/jessa/dr_gccomp_$1_d001.txt 2>&1 &
echo "run in gccomp for dataset $1 and alpha 0.00001 in background"
nohup ./Release/tractableMrfLearner2 gccomp $1 0.00001 /tmp/jessa/ > /tmp/jessa/dr_gccomp_$1_d00001.txt 2>&1 &
top
