################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/featureGeneration/BaselineFeatureGenerator.cpp \
../src/featureGeneration/ClusterFeatureGenerator.cpp \
../src/featureGeneration/HierarchicClusterFeatureGenerator.cpp \
../src/featureGeneration/JavaFeatureGenerator.cpp \
../src/featureGeneration/SimpleClusterFeatureGenerator.cpp 

OBJS += \
./src/featureGeneration/BaselineFeatureGenerator.o \
./src/featureGeneration/ClusterFeatureGenerator.o \
./src/featureGeneration/HierarchicClusterFeatureGenerator.o \
./src/featureGeneration/JavaFeatureGenerator.o \
./src/featureGeneration/SimpleClusterFeatureGenerator.o 

CPP_DEPS += \
./src/featureGeneration/BaselineFeatureGenerator.d \
./src/featureGeneration/ClusterFeatureGenerator.d \
./src/featureGeneration/HierarchicClusterFeatureGenerator.d \
./src/featureGeneration/JavaFeatureGenerator.d \
./src/featureGeneration/SimpleClusterFeatureGenerator.d 


# Each subdirectory must supply rules for building sources it contributes
src/featureGeneration/%.o: ../src/featureGeneration/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: Cross G++ Compiler'
	g++ -D__GXX_EXPERIMENTAL_CXX0X__ -I"/home/jessa/Dropbox/Master2/Thesis/implementation/tractableMrfLearner2/include" -O0 -g3 -Wall -c -fmessage-length=0 -std=c++0x -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


