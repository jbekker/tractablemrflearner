################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/Experiments/alpha_experiments.cpp \
../src/Experiments/helpers.cpp \
../src/Experiments/minimize_gc_experiments.cpp \
../src/Experiments/simple_clustering_fg_experiments.cpp \
../src/Experiments/tree_fg_experiments.cpp 

OBJS += \
./src/Experiments/alpha_experiments.o \
./src/Experiments/helpers.o \
./src/Experiments/minimize_gc_experiments.o \
./src/Experiments/simple_clustering_fg_experiments.o \
./src/Experiments/tree_fg_experiments.o 

CPP_DEPS += \
./src/Experiments/alpha_experiments.d \
./src/Experiments/helpers.d \
./src/Experiments/minimize_gc_experiments.d \
./src/Experiments/simple_clustering_fg_experiments.d \
./src/Experiments/tree_fg_experiments.d 


# Each subdirectory must supply rules for building sources it contributes
src/Experiments/%.o: ../src/Experiments/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: Cross G++ Compiler'
	g++ -D__GXX_EXPERIMENTAL_CXX0X__ -I"/home/jessa/Dropbox/Master2/Thesis/implementation/tractableMrfLearner2/include" -O3 -Wall -c -fmessage-length=0 -std=c++0x -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


