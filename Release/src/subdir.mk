################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/learner.cpp \
../src/log.cpp \
../src/main.cpp \
../src/program.cpp \
../src/sdd_interface.cpp 

OBJS += \
./src/learner.o \
./src/log.o \
./src/main.o \
./src/program.o \
./src/sdd_interface.o 

CPP_DEPS += \
./src/learner.d \
./src/log.d \
./src/main.d \
./src/program.d \
./src/sdd_interface.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: Cross G++ Compiler'
	g++ -D__GXX_EXPERIMENTAL_CXX0X__ -I"/home/jessa/Dropbox/Master2/Thesis/implementation/tractableMrfLearner2/include" -O3 -Wall -c -fmessage-length=0 -std=c++0x -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


