/*
 * learner.h
 *
 *  Created on: 14 Apr 2014
 *      Author: jessa
 */

#ifndef LEARNER_H_
#define LEARNER_H_

#include <string>
#include <vector>
#include <ctime>

#include "Data/Data.h"
#include "featureGeneration/FeatureGenerator.h"
#include "Model/Model.h"
#include "Term/Term.h"
#include "log.h"


struct LearnedModel{
	long double likelihood;
	int nb_iterations;
	long int size;
	string features;
	double cpu_time;
};

LearnedModel learnModel(Data train, Data valid, Data test, FeatureGenerator* fg, double alpha, bool autom, bool minimize, bool keepOnLearning, bool plot, string plotPath);

LearnedModel learnModel(int nbVars, string training_data_path, string validation_data_path, string test_data_path, FeatureGenerator* fg, double alpha, bool autom, bool minimize, bool keepOnLearning, string plotFile);

LearnedModel learnModel(int nbVars, string training_data_path, string validation_data_path, string test_data_path, FeatureGenerator* fg, double alpha, bool keepOnLearning);
LearnedModel learnModelAuto(int nbVars, string training_data_path, string validation_data_path, string test_data_path, FeatureGenerator* fg, double alpha, bool keepOnLearning);
LearnedModel learnModelMinimize(int nbVars, string training_data_path, string validation_data_path, string test_data_path, FeatureGenerator* fg, double alpha, bool keepOnLearning);

LearnedModel learnModel(int nbVars, string training_data_path, string validation_data_path, string test_data_path, FeatureGenerator* fg, double alpha, bool keepOnLearning, string plotfile);
LearnedModel learnModelAuto(int nbVars, string training_data_path, string validation_data_path, string test_data_path, FeatureGenerator* fg, double alpha, bool keepOnLearning, string plotfile);
LearnedModel learnModelMinimize(int nbVars, string training_data_path, string validation_data_path, string test_data_path, FeatureGenerator* fg, double alpha, bool keepOnLearning, string plotfile);

string model_to_string(LearnedModel model);

string to_string(LearnedModel model , string sep);

void plot_write_new(double time, double LL, int size, const string &path);
void plot_write(double time, double LL, int size, const string &path);

#endif /* LEARNER_H_ */
