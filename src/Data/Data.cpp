/*
 * Data.cpp
 *
 *  Created on: 13 Apr 2014
 *      Author: jessa
 */

#include "Data.h"

Data::Data(string path){
	readInSamples(path);
	m_nb_samples = m_features_presence[1].size();
}

Data::~Data() {
}

void Data::readInSamples(string path){

	string line;
	ifstream text (path);

	bool first = true;

	if (text.is_open()){
		while (first && text.good()){
			getline(text,line);
			if(readFirstSample(line))
				first=false;
		}
		while (text.good()){
			getline(text,line);
			readSample(line);
		}
		text.close();
	}
	else{
		std::cout << "Unable to open file " << path << std::endl << std::endl;
	}

}

bool Data::readFirstSample(string line){
	if(line.size()==0)
		return false;


	m_count.push_back(0); //feature 0 does not exist
//	m_count.push_back(1); //workaround

	m_features_presence.push_back(vector<bool>());
	m_features_presence[0].push_back(false); //feature 0 does not exist
//	m_features_presence.push_back(vector<bool>()); //workaround
//	m_features_presence[1].push_back(true); //workaround

	int f = 1; // workaround
//	int f = 2; // workaround
	bool i;
	stringstream ss(line);
	while (ss >> i)
	{
		m_features_presence.push_back(vector<bool>());
		m_features_presence[f].push_back(i);
		m_count.push_back(i);

	    if (ss.peek() == ',')
	        ss.ignore();

	    f++;
	}

	m_nb_vars = f-1;

	return true;
}

void Data::readSample(string line){
	if(line.size()==0)
		return;

//	m_count[1]++; //workaround

	m_features_presence[0].push_back(false); //feature 0 does not exist
//	m_features_presence[1].push_back(true); //workaround

	int f = 1; // workaround
//	int f = 2; // workaround
	bool i;
	stringstream ss(line);
	while (ss >> i)
	{
		m_features_presence[f].push_back(i);
		if(i)
			m_count[f]++;

	    if (ss.peek() == ',')
	        ss.ignore();

	    f++;
	}
}

void Data::addFeature(Term feature){
	int nb_features = getNbFeatures();
	vector<bool> feature_presence;
	int count = 0;

	WmcManager* wmc = wmc_manager_new(feature.toSdd(),0, g_mgr);

	for(int s=0; s<m_nb_samples ; s++){
		bool sat = sample_sat(s, nb_features, wmc);
		feature_presence.push_back(sat);
		count+=sat;
	}

	wmc_manager_free(wmc);

	m_features_presence.push_back(feature_presence);
	m_count.push_back(count);
}

bool Data::sample_sat(int s, int nb_features, WmcManager* wmc){
	for(int f=1 ; f<=nb_features ; f++){
		if (m_features_presence[f][s]){
			wmc_set_literal_weight( f,wmc_one_weight(wmc), wmc);
			wmc_set_literal_weight(-f,wmc_zero_weight(wmc),wmc);
		}
		else{
			wmc_set_literal_weight(f,wmc_zero_weight(wmc),wmc);
			wmc_set_literal_weight(-f,wmc_one_weight(wmc),wmc);
		}
	}
	double count = wmc_propagate(wmc);
	// if evidence is possible in feature, the count will be 2
	// because the variable for the feature is not assigned anything yet,
	// so it can both be true or false.
	// if the evidence is not possible in the feature, the count will be 0.
	return count>0.0001; //epsilon
}



void Data::print() {

cout << "FEATURE PRESENCE: "  << endl;
	for(unsigned int f = 1 ; f<=getNbFeatures() ; f++){
		cout << f << ": ";
		for(int s=0 ; s < getNbSamples() ; s++){
			cout << m_features_presence[f][s];
		}
		cout << " :: " << m_count[f] << endl;
	}
}

void Data::printCount() {

	cout << m_count[1];
	for(unsigned int f = 2 ; f<=getNbFeatures() ; f++){
		cout << " " << m_count[f];
	}
	cout  << endl;
}

void Data::saveAsArff(string path, string relationName){

	ofstream myfile (path);
	if (myfile.is_open()){
		myfile << "@RELATION \"" << relationName << "\"" << endl << endl;

		for(unsigned int i=1; i <= getNbFeatures() ; i++){
			myfile << "@ATTRIBUTE " << i << " {0,1}" << endl;
		}

		myfile << endl << "@DATA";

		for(int s=0 ; s<getNbSamples() ; s++){
			myfile  << endl;
			myfile << m_features_presence[1][s];
			for(unsigned int f=2 ; f<=getNbFeatures() ; f++){
				myfile << ", " << m_features_presence[f][s];
			}
		}

	    myfile.close();
	}
	else cout << "Unable to open file";

}
