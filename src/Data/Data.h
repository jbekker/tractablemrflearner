/*
 * Data.h
 *
 *  Created on: 13 Apr 2014
 *      Author: jessa
 */

#ifndef DATA_H_
#define DATA_H_

#include <string>
#include <vector>
#include <sstream>
#include <iostream>
#include <fstream>
#include <utility>

#include "sdd_interface.h"
#include "../Term/Term.h"

class Data {
private:
	int m_nb_vars;
	int m_nb_samples;
//	bool m_mgr_data;

//	SddManager* m_mgr;

//	vector<SddNode*> m_samples;
	vector<int> m_count;
	vector<vector<bool>> m_features_presence;

	void readInSamples(string path);
	bool readFirstSample(string line);
	void readSample(string line);

	bool sample_sat(int s, int nb_features, WmcManager* wmc);
public:
//	Data(string path, int nb_vars, bool mgr_data);
	Data(string path);
	virtual ~Data();

	int getNbVars() const {return m_nb_vars;}
	int getNbSamples() const {return m_nb_samples;}
	unsigned int getNbFeatures() const {return m_count.size()-1;}

//	vector<SddNode*> getSamples() const {return m_samples;}
	vector<int> getCount() const {return m_count;}
	int* getCountPointer() {return &(m_count[1]);}
	int getCountOf(int f) const  {return m_count[f];}
	vector<vector<bool>> getFeaturesPresence() const {return m_features_presence;}

	void ref();
	void deref();

	void print();
	void printCount();

	void addFeature(Term feature);

	void saveAsArff(string path, string relationName);
};

#endif /* DATA_H_ */
