/*
 * tests.cpp
 *
 *  Created on: 3 May 2014
 *      Author: jessa
 */

#include <iostream>
#include <vector>
#include <string>
#include <list>

#include "Term/Term.h"
#include "Data/Data.h"
#include "Model/Model.h"
#include "learner.h"
#include "Experiments/experiments.h"
#include "log.h"
#include "featureGeneration/FeatureGenerator.h"

using namespace std;

void testTerm() {
	g_mgr = sdd_manager_create(3,0);

	Term t1 = Term(1);
	Term t2 = Term(2);
	Term t3 = Term(3);
	cout << "test" <<endl;

	Term tn3 = Term::neg(t3);
	Term t1a2 = Term::conjoin(t1,t2);
	Term t1on3 = Term::disjoin(t1,tn3);
	Term t2xor3 = Term::xorr(t2,t3);
	Term t1eq2xor3 = Term::equiv(t1,t2xor3);
	cout << "test" <<endl;
	vector<Term> v;
	v.push_back(t1);
	v.push_back(t2);
	v.push_back(t3);
	Term big = Term::equiv(Term::conjoin(v),Term::xorr(t1on3,t1eq2xor3));

	vector<Term> terms;
	terms.push_back(t1);
	terms.push_back(t2);
	terms.push_back(t3);
	terms.push_back(tn3);
	terms.push_back(t1a2);
	terms.push_back(t1on3);
	terms.push_back(t2xor3);
	terms.push_back(t1eq2xor3);
	terms.push_back(big);

	for(Term t : terms){
		cout << t.toString() << endl;
		cout << sdd_to_string(t.toSdd()) << endl << endl;
	}

}

void testData() {
	string datapath = "data/generated1/generated1.test.data";
	g_mgr = sdd_manager_create(6,0);
	Data data = Data(datapath);

	Term feature = Term::conjoin(Term(2),Term(3));
	data.addFeature(feature);

	data.print();
}

void testModel0(){

	g_mgr = sdd_manager_create(6,0);

	Data train("data/generated1/generated1.ts.data");
	Data valid("data/generated1/generated1.valid.data");
	Data test("data/generated1/generated1.test.data");

//	train.ref();
//	valid.ref();
//	test.ref();

	Model model = Model(6,train,valid);

	Model model2 = model;

	model.printTrainData();
	model2.printTrainData();
}

void testModel(){

	g_mgr = sdd_manager_create(6,0);

	Data train("data/generated1/generated1.ts.data");
	Data valid("data/generated1/generated1.valid.data");
	Data test("data/generated1/generated1.test.data");

//	train.ref();
//	valid.ref();
//	test.ref();

	Model model = Model(6,train,valid);
	Term feature = Term::conjoin(Term(2),Term(3));
	Model extendedModel = model.addFeature(feature);

	model.print();
	model.ref();
	extendedModel.ref();
	model.deref();
	sdd_manager_garbage_collect(g_mgr);
	extendedModel.print();

	extendedModel.learnWeights();

	extendedModel.printAllWeightsAndProbabilities();

	cout << extendedModel.getScore(0.15)<< endl;
	cout << extendedModel.calculateLogLikelihood(test) << endl;
	extendedModel.deref();

//	train.deref();
//	valid.deref();
//	test.deref();

	cout <<"einde"<<endl;

}

void testLearner() {

//	int nb_vars = 6;
//	string dataset = "generated1";

	int nb_vars = 32;
	string dataset = "abalone";
	g_mgr = sdd_manager_create(nb_vars,0);

	string train = "data/"+dataset+"/"+dataset+".ts.data";
	string valid = "data/"+dataset+"/"+dataset+".valid.data";
	string test = "data/"+dataset+"/"+dataset+".test.data";

	BaselineFeatureGenerator fg;

	LearnedModel model = learnModel(nb_vars, train, valid, test, &fg, 0.01,false);

	sdd_manager_free(g_mgr);
//	delete mgr;
}

void testSimpleClusterFeatureGenerator() {

	int nb_vars = 5;

	g_mgr = sdd_manager_create(nb_vars,0);
//	g_mgr_data = sdd_manager_create(nb_vars,0);

	string dataset  = "generated1";

	Data train = Data("data/"+dataset+"/"+dataset+".train.data");
	Data valid("data/"+dataset+"/"+dataset+".valid.data");

	Model model = Model(nb_vars,train,valid);
	SimpleClusterFeatureGenerator fg = SimpleClusterFeatureGenerator(0.01, 5);
//	HierarchicClusterFeatureGenerator fg = HierarchicClusterFeatureGenerator(0.01,20);
	fg.initialize(model);
	fg.printPossibilities();
	Term feature = Term::conjoin(Term(2),Term(3));
	feature.addDependency(3);
	feature.addDependency(2);
	model = model.addFeature(feature);
	model.completeTrainCounts();
	fg.hasNext(model);
	fg.next(model);
	fg.mark();
	fg.initialize(model);
	cout << "with new feature " << nb_vars+1 << endl;
	fg.printPossibilities();
}


void dataToArffTest() {
	int nb_vars = 5;
	string dataset  = "generated1";

	string arffPath = "relation1804289383.arff";
	string featuresPath = "features1804289383.txt";
	string treeType = "randomTree";
	string featureGenerationMethod = "prune5";

	Data data = Data("data/"+dataset+"/"+dataset+".train.data");

	data.saveAsArff(arffPath,dataset);

//	system(("java -jar FeatureLearning.jar "+dataset+".arff features.txt prunedTree prune5").c_str());
	system(("java -jar FeatureLearning.jar "+ arffPath + " " + featuresPath +" " + treeType + " " + featureGenerationMethod).c_str());

	system(("cat "+ featuresPath).c_str());
}

void javaFeatureTest() {
	int nb_vars = 16;
	string dataset  = "nltcs";

	g_mgr = sdd_manager_create(nb_vars,0);

	Data train = Data("data/"+dataset+"/"+dataset+".train.data");
	Data valid("data/"+dataset+"/"+dataset+".valid.data");

	Model model = Model(nb_vars,train,valid);

	JavaFeatureGeneratorWithRegeneration fg = JavaFeatureGeneratorWithRegeneration("randomTree", "prune5");

	fg.initialize(model);
	fg.printPossibilities();
	Term feature = Term::conjoin(Term(2),Term(3));
	feature.addDependency(3);
	feature.addDependency(2);
	model = model.addFeature(feature);
	model.completeTrainCounts();
	fg.hasNext(model);
	fg.next(model);
	fg.mark();
	fg.initialize(model);
	cout << "with new feature " << nb_vars+1 << endl;
	fg.printPossibilities();
}
