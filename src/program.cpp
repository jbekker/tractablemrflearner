/*
 * program.cpp

 *
 *  Created on: 10 May 2014
 *      Author: jessa
 */

#include "string"
#include "cstdio";
#include "featureGeneration/FeatureGenerator.h"
#include "Data/Data.h"
#include "log.h"
#include "learner.h"



using namespace std;

struct FG {
	FG(): type("baseline"), arg1(""),arg2(""),arg3(""){}
	string type;
	string arg1;
	string arg2;
	string arg3;
};

struct Configuration {

	Configuration(): dataspath(""), dataset("nltcs"),
			alpha(0.01), autoGcMin(true), minimize(false), keepOnLearning(true),
			plot(false), logPath("notSet"), outputPath("notSet"), outputName("out.csv"), plotPath("notSet"){}

	Configuration(const Configuration& other): dataspath(other.dataspath), dataset(other.dataset),featureGenerator(other.featureGenerator),
			alpha(other.alpha), autoGcMin(other.autoGcMin), minimize(other.minimize), keepOnLearning(other.keepOnLearning),
			plot(other.plot), logPath(other.logPath), outputPath(other.outputPath), outputName(other.outputName), plotPath(other.plotPath){}

	string dataspath;
	string dataset;
	FG featureGenerator;
	double alpha;
	bool autoGcMin;
	bool minimize;
	bool keepOnLearning;
	bool plot;
	string logPath;
	string outputPath;
	string outputName;
	string plotPath;

};

void learn(Configuration& config);
string to_log_path(const Configuration& config);
string to_plot_path(const Configuration& config);
string to_string(const Configuration& config, string sep);
string to_string(const FG& fg, string sep);
string to_datapath(const Configuration& config, string datatype);


void read_next(int argc, char** argv, vector<Configuration>& configs);
void read_newConfig(int argc, char** argv, vector<Configuration>& configs);
void read_dataspath(int argc, char** argv, vector<Configuration>& configs);
void read_dataset(int argc, char** argv, vector<Configuration>& configs);
void read_fg(int argc, char** argv, vector<Configuration>& configs);
void read_alpha(int argc, char** argv, vector<Configuration>& configs);
void read_auto(int argc, char** argv, vector<Configuration>& configs);
void read_min(int argc, char** argv, vector<Configuration>& configs);
void read_keepOnLearning(int argc, char** argv, vector<Configuration>& configs);
void read_plot(int argc, char** argv, vector<Configuration>& configs);
void read_outputPath(int argc, char** argv, vector<Configuration>& configs);
void read_outputName(int argc, char** argv, vector<Configuration>& configs);
void read_plotout(int argc, char** argv, vector<Configuration>& configs);
void read_log(int argc, char** argv, vector<Configuration>& configs);

FeatureGenerator* getFG(FG fg);


string str_newConfig = "-n";
string str_dataspath = "-D";
string str_dataset = "-d";
string str_fg = "-fg";
string str_alpha = "-a";
string str_auto = "-autoGcMin";
string str_min = "-minimize";
string str_KeepOnLearning = "-keepOnLearning";
string str_plot = "-plot";
string str_outputName = "-out";
string str_outputPath = "-OUT";
string str_plotout = "-plotout";
string str_log = "-log";


void learn(Configuration& config) {
	cout << to_string(config," ") << endl;
	if(config.outputPath=="notSet")
		config.outputPath="";
	if(config.logPath=="notSet")
		config.logPath=config.outputPath;
	if(config.plotPath=="notSet")
		config.plotPath=config.outputPath;

	log_set_path(to_log_path(config));


	Data train = Data(to_datapath(config, "train"));
	Data valid = Data(to_datapath(config, "valid"));
	Data test = Data(to_datapath(config, "test"));

	FeatureGenerator* fg = getFG(config.featureGenerator);
	LearnedModel model = learnModel(train, valid, test, fg, config.alpha, config.autoGcMin,config.minimize, config.keepOnLearning, config.plot, to_plot_path(config));
	delete fg;

    std::ofstream file(
    		config.outputPath + config.outputName, std::ios_base::out | std::ios_base::app );
    string sep = ";";
    file << to_string(config, sep) + sep + to_string(model, sep)<< endl;
}

string to_log_path(const Configuration& config){
	return config.logPath + "log_" + to_string(config, "_");
}

string to_plot_path(const Configuration& config) {
	return config.plotPath + "plot_" + to_string(config, "_");
}

string to_string(const Configuration& config, string sep){
	return config.dataset+ sep +
			to_string(config.alpha)+sep+
			to_string(config.autoGcMin)+sep+
			to_string(config.minimize)+sep+
			to_string(config.keepOnLearning)+sep+
			to_string(config.plot)+sep+
			to_string(config.featureGenerator,sep);
}

string to_string(const FG& fg, string sep){
	return fg.type+ sep +
			fg.arg1+sep+
			fg.arg2+sep+
			fg.arg3;
}


string to_datapath(const Configuration& config, string datatype){
	return config.dataspath+"/"+config.dataset+"."+datatype+".data";
}


FeatureGenerator* getFG(FG fg){
	if (fg.type=="baseline")
		return new BaselineFeatureGenerator();
	if (fg.type=="cluster" && fg.arg1=="simple")
		return new SimpleClusterFeatureGenerator(stod(fg.arg2),stoi(fg.arg3));
	if (fg.type=="cluster" && fg.arg1=="hierarchic")
		return new SimpleClusterFeatureGenerator(stod(fg.arg2),stoi(fg.arg3));
	if (fg.type=="tree" && stoi(fg.arg3)==0)
		return new JavaFeatureGeneratorWithoutRegeneration(fg.arg1, fg.arg2);
	if (fg.type=="tree" && stoi(fg.arg3)!=0)
		return new JavaFeatureGeneratorWithRegeneration(fg.arg1, fg.arg2);

	cout << "Not coorect feature generator" << endl;
	throw "Not coorect feature generator";
}

void read_next(int argc, char** argv, vector<Configuration>& configs){
	if (argc>0){
		string arg = string(argv[0]);
		if(arg == str_newConfig)
			read_newConfig(argc-1, &argv[1],configs);
		else if(arg == str_dataspath)
			read_dataspath(argc-1, &argv[1],configs);
		else if(arg == str_dataset)
			read_dataset(argc-1, &argv[1],configs);
		else if(arg == str_fg)
			read_fg(argc-1, &argv[1],configs);
		else if(arg == str_alpha)
			read_alpha(argc-1, &argv[1],configs);
		else if(arg == str_auto)
			read_auto(argc-1, &argv[1],configs);
		else if(arg == str_min)
			read_min(argc-1, &argv[1],configs);
		else if(arg == str_KeepOnLearning)
			read_keepOnLearning(argc-1, &argv[1],configs);
		else if(arg == str_plot)
			read_plot(argc-1, &argv[1],configs);
		else if(arg == str_outputPath)
			read_outputPath(argc-1, &argv[1],configs);
		else if(arg == str_outputName)
			read_outputName(argc-1, &argv[1],configs);
		else if(arg == str_plotout)
			read_plotout(argc-1, &argv[1],configs);
		else if(arg == str_log)
			read_log(argc-1, &argv[1],configs);
		else{
			cout << arg + " is not a correct option!" << endl;
			throw arg + " is not a correct option!";
		}
	}
}

void read_newConfig(int argc, char** argv, vector<Configuration>& configs){
	if(configs.empty()){
		Configuration newConfiguration = Configuration();
		configs.push_back(newConfiguration);
	}
	else{
		Configuration newConfiguration(configs.back());
		configs.push_back(newConfiguration);
	}
	read_next (argc, argv, configs);
}

void read_dataspath(int argc, char** argv, vector<Configuration>& configs){
	if (argc < 1){
		cout << "dataspath was not given after " << endl;
		throw "dataspath was not given after " + str_dataspath;
	}

	configs.back().dataspath = string(argv[0]);

	read_next(argc-1, &argv[1], configs);
}

void read_dataset(int argc, char** argv, vector<Configuration>& configs){
	if (argc < 1){
		cout << "dataset was not given after " + str_dataset << endl;
		throw "dataset was not given after " + str_dataset;
	}

	configs.back().dataset= string(argv[0]);

	read_next(argc-1, &argv[1], configs);
}

void read_fg(int argc, char** argv, vector<Configuration>& configs){
	if (argc <1){
		cout <<  "feature generator was not given after " + str_dataset << endl;
		throw "feature generator was not given after " + str_dataset;
	}
	string type = string(argv[0]);

	if(type == "baseline"){
		FG fg;
		fg.type = "baseline";
		configs.back().featureGenerator=fg;
		return read_next(argc-1, &argv[1],configs);
	}

	if(type == "cluster"){
		if(argc<4){
			cout << "not enough arguments given for feature generator cluster" << endl;
			throw "not enough arguments given for feature generator cluster" ;
		}

		FG fg;
		fg.type = "cluster";
		fg.arg1 = string(argv[1]);
		fg.arg2 = string(argv[2]);
		fg.arg3 = string(argv[3]);
		configs.back().featureGenerator=fg;

		return read_next(argc-4, &argv[4],configs);
	}

	if(type == "tree"){
		if(argc<4){
			cout << "not enough arguments given for feature generator cluster"  << endl;
			throw "not enough arguments given for feature generator cluster" ;
		}
		FG fg;
		fg.type = "tree";
		fg.arg1 = string(argv[1]);
		fg.arg2 = string(argv[2]);
		fg.arg3 = string(argv[3]);
		configs.back().featureGenerator=fg;

		return read_next(argc-4, &argv[4],configs);
	}
}


void read_alpha(int argc, char** argv, vector<Configuration>& configs){
	if (argc <1){
		cout << "alpha was not given after " + str_alpha << endl;
		throw "alpha was not given after " + str_alpha;
	}
	configs.back().alpha=stod(argv[0]);
	read_next(argc-1, &argv[1], configs);
}

void read_auto(int argc, char** argv, vector<Configuration>& configs) {
	if (argc <1){
		cout << "auto garbage collection and minimize boolean was not given after " + str_auto << endl;
		throw "auto garbage collection and minimize boolean was not given after " + str_auto;
	}
	configs.back().autoGcMin=stoi(argv[0]);
	read_next(argc-1, &argv[1], configs);
}

void read_min(int argc, char** argv, vector<Configuration>& configs) {
	if (argc <1){
		cout << "minimize boolean was not given after " + str_min << endl;
		throw "minimize boolean was not given after " + str_min;
	}
	configs.back().minimize=stoi(argv[0]);
	read_next(argc-1, &argv[1], configs);
}

void read_keepOnLearning(int argc, char** argv, vector<Configuration>& configs) {
	if (argc <1){
		cout << "keep on learning boolean was not given after " + str_KeepOnLearning << endl;
		throw "keep on learning boolean was not given after " + str_KeepOnLearning;
	}
	configs.back().keepOnLearning = stoi(argv[0]);
	read_next(argc-1, &argv[1], configs);
}

void read_plot(int argc, char** argv, vector<Configuration>& configs) {
	if (argc <1){
		cout << "plot boolean was not given after " + str_plot << endl;
		throw "plot boolean was not given after " + str_plot;
	}
	configs.back().plot=stoi(argv[0]);
	read_next(argc-1, &argv[1], configs);
}

void read_outputPath(int argc, char** argv, vector<Configuration>& configs) {
	if (argc <1){
		cout << "outputpath was not given after " + str_outputPath << endl;
		throw "outputpath was not given after " + str_outputPath;
	}
	configs.back().outputPath=argv[0];
	read_next(argc-1, &argv[1], configs);
}

void read_outputName(int argc, char** argv, vector<Configuration>& configs) {
	if (argc <1){
		cout << "outputpath was not given after " + str_outputPath << endl;
		throw "outputpath was not given after " + str_outputPath;
	}
	configs.back().outputName=argv[0];
	read_next(argc-1, &argv[1], configs);
}


void read_plotout(int argc, char** argv, vector<Configuration>& configs){
	if (argc <1){
		cout << "plot output path was not given after " + str_plotout << endl;
		throw "plot output path was not given after " + str_plotout;
	}
	configs.back().plotPath=argv[0];
	read_next(argc-1, &argv[1], configs);
}
void read_log(int argc, char** argv, vector<Configuration>& configs){
	if (argc <1){
		cout << "log path was not given after " + str_log << endl;
		throw "log path was not given after " + str_log;
	}
	configs.back().logPath=argv[0];
	read_next(argc-1, &argv[1], configs);
}

int main(int argc, char** argv) {

	// remove first arg
	argc--;
	argv = &argv[1];

	// remove second arg if it is -n
	if(argc>0 && argv[0]== str_newConfig){
		argc--;
		argv = &argv[1];
	}

	vector<Configuration>
	configs;
	read_newConfig(argc, argv, configs);

	for(Configuration config : configs){
		learn(config);
	}

	return 0;

}
