/*
 * Term.h
 *
 *  Created on: 13 Apr 2014
 *      Author: jessa
 */

#ifndef TERM_H_
#define TERM_H_

#include <string>
#include <vector>
#include <set>
#include "sdd_interface.h"
using namespace std;

class Term {
private:
	SddNode* m_sdd;
	string m_str;
	bool m_isConstant;
	set<int> m_dependencies;

public:
	Term():m_dependencies(set<int>()){}
	Term(SddNode* sdd, string str, bool is_Constant): m_sdd(sdd), m_str(str), m_isConstant(is_Constant),m_dependencies(set<int>()){}
	Term(int literal);
	virtual ~Term();

	void setDependencies(set<int> dependencies){m_dependencies = dependencies;}
	void addDependency(int dependency){m_dependencies.insert(dependency);}
	void addDependencies(set<int> dependencies){m_dependencies.insert(dependencies.begin(),dependencies.end());}
	set<int> getDependencies() const {return m_dependencies;}

	SddNode* toSdd() {return m_sdd;}
	string toString() {return m_str;}
	bool isConstant() {return m_isConstant;}

	void ref() {sdd_ref(m_sdd, g_mgr);}
	void deref() {sdd_deref(m_sdd, g_mgr);}

	bool equals(const Term other) const {return (m_str == other.m_str);}

	static Term T();
	static Term F();

	static Term neg(Term t);
	static Term conjoin(vector<Term> terms);
	static Term conjoin(set<Term> terms);
	static Term conjoin(int t1, set<int> terms);
	static Term conjoin(Term t1, Term t2);
	static Term disjoin(vector<Term> terms);
	static Term disjoin(Term t1, Term t2);
	static Term imply(Term t1, Term t2);
	static Term equiv(Term t1, Term t2);
	static Term xorr(Term t1, Term t2);
};

#endif /* TERM_H_ */
