/*
 * Term.cpp
 *
 *  Created on: 13 Apr 2014
 *      Author: jessa
 */

#include "Term.h"

Term::Term(int literal):m_dependencies(set<int>()){
	if(literal == 0){
		m_isConstant = false;
		m_sdd = sdd_manager_true(g_mgr);
		m_str = "T";
	}
	else{
		m_isConstant = true;
		m_sdd = sdd_manager_literal(literal, g_mgr);
		m_str = to_string(literal);
	}
}

Term::~Term() {
}



Term Term::T(){
	return Term(sdd_manager_true(g_mgr),"T", false);
}

Term Term::F(){
	return Term(sdd_manager_false(g_mgr),"F", false);
}

Term Term::neg(Term t){
	SddNode* sdd = sdd_negate(t.m_sdd,g_mgr);
	string str = "( - " + t.m_str + " )";
	return Term(sdd,str, false);
}

Term Term::conjoin(vector<Term> terms){
		if(terms.size()>0){

			for(Term t : terms)
				t.ref();

			SddNode* sdd = sdd_manager_true(g_mgr);
			string str = "( and";
			for(Term t : terms){
				sdd = sdd_conjoin(sdd, t.m_sdd,g_mgr);
				str = str+ " " + t.m_str;
			}
			str = str + " )";

			for(Term t : terms)
				t.deref();

			return Term(sdd,str,false);
		}
		else{
			return Term(sdd_manager_true(g_mgr), "T", false);
		}
}



Term Term::conjoin(set<Term> terms){
		if(terms.size()>0){

			for(Term t : terms)
				t.ref();

			SddNode* sdd = sdd_manager_true(g_mgr);
			string str = "( and";
			for(Term t : terms){
				sdd = sdd_conjoin(sdd, t.m_sdd,g_mgr);
				str = str+ " " + t.m_str;
			}
			str = str + " )";

			for(Term t : terms)
				t.deref();

			return Term(sdd,str,false);
		}
		else{
			return Term(sdd_manager_true(g_mgr), "T", false);
		}
}


Term Term::conjoin(Term t1, Term t2){
	vector<Term> terms;
	terms.push_back(t1);
	terms.push_back(t2);
	return conjoin(terms);
}

Term Term::conjoin(int t1, set<int> others){
	vector<Term> terms;
	terms.push_back(Term(t1));
	for(int t : others){
		terms.push_back(Term(t));
	}
	return conjoin(terms);
}

Term Term::disjoin(vector<Term> terms){
		if(terms.size()>0){

			for(Term t : terms)
				t.ref();

			SddNode* sdd = sdd_manager_false(g_mgr);
			string str = "( or";
			for(Term t : terms){
				sdd = sdd_disjoin(sdd, t.m_sdd,g_mgr);
				str = str+ " " + t.m_str;
			}
			str = str + " )";

			for(Term t : terms)
				t.deref();

			return Term(sdd,str,false);
		}
		else{
			return Term(sdd_manager_true(g_mgr),"T",false);
		}
}

Term Term::disjoin(Term t1, Term t2){
	vector<Term> terms;
	terms.push_back(t1);
	terms.push_back(t2);
	return disjoin(terms);
}


Term Term::imply(Term t1, Term t2){
	string str = "( " + t1.m_str + " => " + t2.m_str + " )";
	SddNode* sdd = sdd_imply(t1.m_sdd, t2.m_sdd, g_mgr);
	return Term(sdd,str,false);
}

Term Term::equiv(Term t1, Term t2){
	string str = "( " + t1.m_str + " <=> " + t2.m_str + " )";
	SddNode* sdd = sdd_equiv(t1.m_sdd, t2.m_sdd, g_mgr);
	return Term(sdd,str,false);
}

Term Term::xorr(Term t1, Term t2){
	string str = "( " + t1.m_str + " xor " + t2.m_str + " )";
	SddNode* sdd = sdd_xor(t1.m_sdd, t2.m_sdd, g_mgr);
	return Term(sdd,str,false);
}
