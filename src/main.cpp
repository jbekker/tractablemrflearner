///* * main.cpp
// *
// *  Created on: 13 Apr 2014
// *      Author: jessa
// */
//#include <iostream>
//#include <vector>
//#include <string>
//#include <list>
//
//#include "Term/Term.h"
//#include "Data/Data.h"
//#include "Model/Model.h"
//#include "learner.h"
//#include "Experiments/experiments.h"
//#include "log.h"
//#include "tests.h";
//using namespace std;
//
///*
// * Test different alpha values for baseline feature generator
// *
// * usage:
// * ./Release/tractableMrfLearner2 alpha dataset alpha_first_alpha alpha_last log_path
// *
// * alpha:		which experiment
// * dataset:		which dataset
// * alpha_first:	the first value form the array with alpha values to use. Array is defined in alpha_experiments
// * alpha_last:	the last value form the array with alpha values to use. Array is defined in alpha_experiments
// * log_path: 	in which folder the log will be savedt pu
// *
// * eg: ./Release/tractableMrfLearner2 alpha nltcs 0 3 /tmp/jessa/
// * test alpha for values 0 to 3 (4 in total) on dataset nltcs, write log to /tmp/jessa
// */
//void alphaExperiment(int argc, char** argv){
//	string dataset;
//	string alpha_first;
//	string alpha_last;
//	string log_path;
//	bool plot;
//
//	if(argc ==0){
//		dataset = "generated1";
//		alpha_first = "7";
//		alpha_last = "7";
//		log_path = "";
//		plot = true;
//	}
//	else{
//		dataset = argv[1];
//		alpha_first = argv[2];
//		alpha_last = argv[3];
//		log_path = argv[4];
//		plot = argv[5];
//	}
//
//	log_set_path(log_path + "log_" + dataset + "_" + alpha_first + "_" + alpha_last + ".txt");
//	runAlphaExperiment(dataset, stoi(alpha_first),stoi(alpha_last),plot);
//}
//
///*
// * Test different alpha values for baseline feature generator while minimization is used
// *
// * usage:
// * ./Release/tractableMrfLearner2 minimize dataset alpha_first alpha_last log_path
// *
// * minimize:	which experiment
// * dataset:		which dataset
// * alpha_first:	the first value form the array with alpha values to use. Array is defined in minimize_gc_experiments
// * alpha_last:	the last value form the array with alpha values to use. Array is defined in minimize_gc_experiments
// * log_path: 	in which folder the log will be saved
// *
// * eg: ./Release/tractableMrfLearner2 minimize nltcs 0 3 /tmp/jessa/
// * test alpha for values 0 to 3 (4 in total) on dataset nltcs, using minimization, write log to /tmp/jessa
// */
//void minimizeExperiment(int argc, char** argv){
//	string dataset;
//	string alpha_first;
//	string alpha_last;
//	string log_path;
//
//	if(argc ==0){
//		dataset = "generated1";
//		alpha_first = "2";
//		alpha_last = "2";
//		log_path = "";
//	}
//	else{
//		dataset = argv[1];
//		alpha_first = argv[2];
//		alpha_last = argv[3];
//		log_path = argv[4];
//	}
//
//	log_set_path(log_path + "log_minimize_" + dataset + ".txt");
//	runMinimizeExperiment(dataset,stoi(alpha_first), stoi(alpha_last));
//}
//
///*
// * Test different alpha values for baseline feature generator while automatic garbage collection and minimization is used
// *
// * usage:
// * ./Release/tractableMrfLearner2 auto dataset alpha_first alpha_last log_path
// *
// * auto:			which experiment
// * dataset:		which dataset
// * alpha_first:	the first value form the array with alpha values to use. Array is defined in minimize_gc_experiments
// * alpha_last:	the last value form the array with alpha values to use. Array is defined in minimize_gc_experiments
// * log_path: 	in which folder the log will be saved
// *
// * eg: ./Release/tractableMrfLearner2 auto nltcs 0 3 /tmp/jessa/
// * test alpha for values 0 to 3 (4 in total) on dataset nltcs, using automatic garbage collection and minimization, write log to /tmp/jessa
// */
//void autoExperiment(int argc, char** argv){
//	string dataset;
//	string alpha_first;
//	string alpha_last;
//	string log_path;
//
//	if(argc ==0){
//		dataset = "generated1";
//		alpha_first = "2";
//		alpha_last = "2";
//		log_path = "";
//	}
//	else{
//		dataset = argv[1];
//		alpha_first = argv[2];
//		alpha_last = argv[3];
//		log_path = argv[4];
//	}
//
//	log_set_path(log_path + "log_minimize_" + dataset + ".txt");
//	runAutoExperiment(dataset,stoi(alpha_first), stoi(alpha_last));
//}
//
//void gcCompExperiment(int argc, char** argv){
//	string dataset;
//	string alpha;
//	string log_path;
//
//	if(argc ==0){
//		dataset = "generated1";
//		alpha = "0.01";
//		log_path = "";
//	}
//	else{
//		dataset = argv[1];
//		alpha= argv[2];
//		log_path = argv[3];
//	}
//
//	log_set_path(log_path + "log_minimize_" + dataset + ".txt");
//	runGcComparationExperiment(dataset,stod(alpha));
//}
//
//void gcMinExperiment(int argc, char** argv){
//	string dataset;
//	string alpha;
//	string log_path;
//
//	if(argc ==0){
//		dataset = "generated1";
//		alpha = "0.01";
//		log_path = "";
//	}
//	else{
//		dataset = argv[1];
//		alpha= argv[2];
//		log_path = argv[3];
//	}
//
//	log_set_path(log_path + "log_minimize_" + dataset + ".txt");
//	runMinimizeExperiment(dataset,stod(alpha));
//}
//
///*
// * Test different treshold values for simple cluster feature generator
// *
// * usage:
// * ./Release/tractableMrfLearner2 treshold dataset treshold_first treshold_last log_path alpha
// *
// * treshold:			which experiment
// * dataset:			which dataset
// * treshold_first:	the first value form the array with treshold values to use. Array is defined in simple_clustering_fg_experiments
// * treshold_last:	the last value form the array with treshold values to use. Array is defined in simple_clustering_fg_experiments
// * log_path: 		in which folder the log will be saved
// * alpha:			which alpha value to use
// *
// * eg: ./Release/tractableMrfLearner2 treshold nltcs 0 3 /tmp/jessa/ 0.001
// * test treshold for values 0 to 3 (4 in total) on dataset nltcs, using 0.001 as alpha, write log to /tmp/jessa
// */
//void simple_clust_treshold_experiment(int argc, char** argv){
//		string dataset;
//		string treshold_first;
//		string treshold_last;
//		string log_path;
//		string alpha;
//
//		if(argc ==0){
//			dataset = "generated1";
//			treshold_first = "0";
//			treshold_last = "5";
//			log_path = "";
//			alpha = "0.001";
//		}
//		else{
//			dataset = argv[1];
//			treshold_first = argv[2];
//			treshold_last = argv[3];
//			log_path = argv[4];
//			alpha = argv[5];
//		}
//
//	log_set_path(log_path + "log_" + dataset + "_" + treshold_first + "_" + treshold_last + ".txt");
//	runSimpleClusteringTresholdExperiment(dataset,stod(alpha),stoi(treshold_first), stoi(treshold_last));
//
//}
//
///*
// * Test different limit values for simple cluster feature generator
// *
// * usage:
// * ./Release/tractableMrfLearner2 limit dataset limit_first limit_last log_path alpha
// *
// * treshold:		which experiment
// * dataset:		which dataset
// * limit_first:	the first value form the array with limit values to use. Array is defined in simple_clustering_fg_experiments
// * limit_last:	the last value form the array with limit values to use. Array is defined in simple_clustering_fg_experiments
// * log_path: 	in which folder the log will be saved
// * alpha:		which alpha value to use
// *
// * eg: ./Release/tractableMrfLearner2 treshold nltcs 0 3 /tmp/jessa/ 0.001
// * test limit for values 0 to 3 (4 in total) on dataset nltcs, using 0.001 as alpha, write log to /tmp/jessa
// */
//void simple_clust_limit_experiment(int argc, char** argv){
//	string dataset;
//	string limit_first;
//	string limit_last;
//	string log_path;
//	string alpha;
//
//	if(argc ==0){
//		dataset = "nltcs";
//		limit_first = "0";
//		limit_last = "3";
//		log_path = "";
//		alpha = "0.001";
//	}
//	else{
//		dataset = argv[1];
//		limit_first = argv[2];
//		limit_last = argv[3];
//		log_path = argv[4];
//		alpha = argv[5];
//	}
//
//	log_set_path(log_path + "log_" + dataset + "_" + limit_first + "_" + limit_last + ".txt");
//	runSimpleClusteringLimitExperiment(dataset,stod(alpha),stoi(limit_first), stoi(limit_last));
//
//}
//
///*
// * Test different treshold and limit combinationsfor simple cluster feature generator
// *
// * usage:
// * ./Release/tractableMrfLearner2 treshold dataset treshold_first treshold_last limit_first limit_last log_path alpha
// *
// * treshold:			which experiment
// * dataset:			which dataset
// * treshold_first:	the first value form the array with treshold values to use. Array is defined in simple_clustering_fg_experiments
// * treshold_last:	the last value form the array with treshold values to use. Array is defined in simple_clustering_fg_experiments
// * limit_first:		the first value form the array with limit values to use. Array is defined in simple_clustering_fg_experiments
// * limit_last:		the last value form the array with limit values to use. Array is defined in simple_clustering_fg_experiments
// * log_path: 		in which folder the log will be saved
// * alpha:			which alpha value to use
// *
// * eg: ./Release/tractableMrfLearner2 treshold nltcs 0 3 0 2 /tmp/jessa/ 0.001
// * test treshold for values 0 to 3 (4 in total) in combination with limit values 0 to 2 (3 in total) on dataset nltcs, using 0.001 as alpha, write log to /tmp/jessa
// */
//void simple_clust_limit_treshold_experiment(int argc, char** argv){
//	string dataset;
//	string treshold_first;
//	string treshold_last;
//	string limit_first;
//	string limit_last;
//	string log_path;
//	string alpha;
//
//	if(argc ==0){
//		dataset = "nltcs";
//		treshold_first = "2";
//		treshold_last = "3";
//		limit_first = "0";
//		limit_last = "3";
//		log_path = "";
//		alpha = "0.001";
//	}
//	else{
//		dataset = argv[1];
//		treshold_first = argv[2];
//		treshold_last = argv[3];
//		limit_first = argv[4];
//		limit_last = argv[5];
//		log_path = argv[6];
//		alpha = argv[7];
//	}
//	log_set_path(log_path + "log_" + dataset + "_" + treshold_first + "_" + treshold_last + "_" + limit_first + "_" + limit_last + ".txt");
//	runSimpleClusteringTresholdLimigExperiment(dataset,stod(alpha), stoi(treshold_first), stoi(treshold_last), stoi(limit_first), stoi(limit_last));
//
//}
//
///*
// * Test different tree feature generators
// *
// * usage:
// * ./Release/tractableMrfLearner2 tree dataset experiment_first experiment_last log_path alpha
// *
// * tree:			which experiment
// * dataset:			which dataset
// * experiment_first:the first experiment to execute, from possibilities in array, defined in tree_fg_experiments
// * experiment_last:	the last experiment to execute, from possibilities in array, defined in tree_fg_experiments
// * log_path: 		in which folder the log will be saved
// * alpha:			which alpha value to use
// *
// * eg: ./Release/tractableMrfLearner2 tree nltcs 0 6 /tmp/jessa/ 0.001
// * do tree experiments 0 to 7 on dataset nltcs, using 0.001 as alpha, write log to /tmp/jessa
// */
//void tree_experiment(int argc, char** argv){
//	string dataset;
//	string experiment_first;
//	string experiment_last;
//	string log_path;
//	string alpha;
//
//	if(argc ==0){
//		dataset = "generated1";
//		experiment_first = "0";
//		experiment_last = "6";
//		log_path = "";
//		alpha = "0.001";
//	}
//	else{
//		dataset = argv[1];
//		experiment_first = argv[2];
//		experiment_last = argv[3];
//		log_path = argv[4];
//		alpha = argv[5];
//	}
//	log_set_path(log_path + "log_" + dataset + "_tree_" + experiment_first + "_" + experiment_last + ".txt");
//	runTreeExperiment(dataset,stod(alpha),stoi(experiment_first), stoi(experiment_last));
//}
//
//int main(int argc, char** argv) {
//	sdd_manager_free(g_mgr);
//
//	if (argc > 1){ //argumenten meegegeven -> via command line
//		if (string(argv[1])=="alpha"){
//			alphaExperiment(argc-1,&(argv[1]));
//		}
//		if (string(argv[1])=="minimize"){
//			cout << "MINIMIZE EXPERIMET" << endl;
//			minimizeExperiment(argc-1,&(argv[1]));
//		}
//		if (string(argv[1])=="auto"){
//			cout << "AUTO GC AND MINIMIZE EXPERIMET" << endl;
//			autoExperiment(argc-1,&(argv[1]));
//		}
//
//		if (string(argv[1])=="gccomp"){
//			gcCompExperiment(argc-1,&(argv[1]));
//		}
//
//		if (string(argv[1])=="minimize2"){
//			gcMinExperiment(argc-1,&(argv[1]));
//		}
//
//		if (string(argv[1]) == "limit"){
//			simple_clust_limit_experiment(argc-1,&(argv[1]));
//		}
//		if (string(argv[1]) == "treshold"){
//			simple_clust_treshold_experiment(argc-1,&(argv[1]));
//		}
//		if(string(argv[1]) == "combo") {
//			simple_clust_limit_treshold_experiment(argc-1,&(argv[1]));
//		}
//		if(string(argv[1]) == "tree") {
//			tree_experiment(argc-1,&(argv[1]));
//		}
//	}
//
//	else{ // wat ik in eclipse wil testen
//
////				autoExperiment(argc-1,&(argv[1]));
//				alphaExperiment(argc-1,&(argv[1]));
////				minimizeExperiment(argc-1,&(argv[1]));
//		//		simple_clust_treshold_experiment(argc, argv);
//
////		javaFeatureTest();
////		dataToArffTest();
////		tree_experiment(argc-1,&(argv[1]));
//	}
//
//
//	return 0;
//}
