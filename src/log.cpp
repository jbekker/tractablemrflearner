/*
 * log.cpp
 *
 *  Created on: 16 Apr 2014
 *      Author: jessa
 */

#include "log.h"
#include <iostream>

int g_log_cur_indent_length = 0;
std::string g_log_cur_indent = "";
std::string g_log_file_path = "log.txt";


void log_write( const std::string &text )
{
    std::ofstream log_file(
    		g_log_file_path, std::ios_base::out | std::ios_base::app );
    log_file << g_log_cur_indent << text << std::endl;
//    std::cout << g_log_cur_indent << text << std::endl;
}

void log_set_path(std::string path){
	g_log_file_path = path;
}

void log_add_line(){
	log_write("--------------------------------------------------------------------------");
}

void log_white_line(){
	log_write("");
}

void log_indent(){
	g_log_cur_indent_length++;
	g_log_cur_indent += "  ";
}

void log_dindent() {
	if(g_log_cur_indent_length>0){
		g_log_cur_indent_length--;
		g_log_cur_indent="";
		for(unsigned int i=0 ; i<g_log_cur_indent_length; i++){
			g_log_cur_indent += "  ";
		}
	}

}
