/*
 * learner.cpp
 *
 *  Created on: 14 Apr 2014
 *      Author: jessa
 */

#include "learner.h"

bool debug_L = false;
bool debug_l = false;



LearnedModel learnModel(int nbVars, string training_data_path, string validation_data_path, string test_data_path, FeatureGenerator* fg, double alpha, bool autom, bool minimize, bool keepOnLearning, string plotFile){

	return learnModel(Data(training_data_path), Data(validation_data_path), Data(test_data_path), fg, alpha, autom, minimize, keepOnLearning, (plotFile!=""), plotFile);
}


LearnedModel learnModel(Data training_data, Data validation_data, Data test_data, FeatureGenerator* fg, double alpha, bool autom, bool minimize, bool keepOnLearning, bool plot, string plotFile){

	int nbVars = training_data.getNbVars();

	if(autom)
		minimize=false;

	plotFile = "experimentResults/" + plotFile;
	g_mgr = sdd_manager_create(nbVars,autom);

	log_write("training and validation read");


	double tstart = clock();
	double time=(clock()-tstart)/CLOCKS_PER_SEC;

	log_write("initialization");
	//initialization;
	Model model = Model(nbVars,training_data,validation_data);
	Model bestModel = model;
	model.ref(); //ref twice because should still exist after a better one is found to generate a new option
	bestModel.ref();
	double bestLL = bestModel.getValidationLogLikelihood();
	double bestScore = bestModel.getScore(alpha,bestLL);
	log_write("initial score: " + to_string(bestScore));
	if(plot){
			plot_write_new(0,bestLL,model.getSize(),plotFile);
	}

	bool stop = false;
	bool noBetterScoreYet=false;
	int iteration = 0;
//	double maxNbOfSecs = 7200;
	double maxNbOfSecs = 1800*log2(double(nbVars));

 	while(!stop && time < maxNbOfSecs){

		log_add_line();
		log_white_line();
		iteration++;
		stop = true; // model has not yet changes this iteration
		noBetterScoreYet = true;

		log_write("iteration; " + to_string(iteration));

		fg->initialize(model);

		int f = 0;
		log_indent();
		while(fg->hasNext(model)){
			log_white_line();
			Term feature = fg->next(model);
			feature.ref();

			if(debug_L)
				cout << feature.toString() << endl;
			log_write("add feature: " + feature.toString());

			// generate new model
			Model newModel = model.addFeature(feature);
			newModel.ref();
			feature.deref();
			if (minimize)
				newModel.minimize();

			newModel.learnWeights();

			double LL = newModel.getValidationLogLikelihood();
			double score = newModel.getScore(alpha,LL);
			if(debug_L){
				newModel.printAllWeightsAndProbabilities();
				cout << score << endl;
			}
			log_write("score: " + to_string(score));

			// new model is better?
			if(score > bestScore){
				log_write("better with feature " + feature.toString() + " : " + to_string(score) + ">" + to_string(bestScore));

				bestModel.deref();
				bestLL = LL; // important for LL-time plot
				bestScore = score;
				bestModel = newModel;
				fg->mark();
				stop = false; // model has changed this iteration
				noBetterScoreYet = false; // model with better score was found
			}
			else if(keepOnLearning && noBetterScoreYet && LL>bestLL){
				log_write("better LL (not score) with feature " + feature.toString() + " : " + to_string(score) + ">" + to_string(bestScore));
				bestModel.deref();
				bestLL = LL;
				// don't change best score!
				bestModel=newModel;
				fg->mark();
				stop = false;  // model has changed this iteration
			}
			else{
				newModel.deref();
			}

			if(!autom){
				if(sdd_manager_garbage_collect_if(0.75, g_mgr))
					log_write("garbage collected in mgr_model");
			}

			log_write("sdd_size: " + to_string(model.getSize()));
			log_write("sdd_mgr_size: " + to_string(sdd_manager_size(g_mgr)) + "(live: " + to_string(sdd_manager_live_size(g_mgr)) + ")");
			log_write("sdd_count: " + to_string(model.getCount()));
			log_write("sdd_mgr_count: " + to_string(sdd_manager_count(g_mgr)) + "(live: " + to_string(sdd_manager_live_count(g_mgr)) + ")");

//			sdd_manager_print(g_mgr);
			log_write(sdd_to_string(g_mgr));
			f++;
		}
		log_dindent();

		model.deref(); //deref twice because ref twice (because should still exist after a better one is found to generate a new option)
		if(!stop){
			model = bestModel;
			model.ref(); //ref twice because should still exist after a better one is found to generate a new option
		}
		else
			iteration--;

		if(!autom){
			sdd_manager_garbage_collect(g_mgr);
			log_write("garbage collection executed");
		}

		log_write("chosen feature: " + bestModel.getMostRecentFeature().toString());
		log_write("with score: " + to_string(bestScore));
		log_write("sdd_size: " + to_string(model.getSize()));
		log_write("sdd_mgr_size: " + to_string(sdd_manager_size(g_mgr)) + "(live: " + to_string(sdd_manager_live_size(g_mgr)) + ")");
		log_write("sdd_count: " + to_string(model.getCount()));
		log_write("sdd_mgr_count: " + to_string(sdd_manager_count(g_mgr)) + "(live: " + to_string(sdd_manager_live_count(g_mgr)) + ")");

		time = (clock()-tstart)/CLOCKS_PER_SEC;

		if(plot){
				plot_write(time,bestLL,model.getSize(),plotFile);
		}
	}

	log_add_line();
	log_white_line();

	model.minimize(); // NEW

	// prepare output

	LearnedModel learnedModel;
	learnedModel.likelihood = model.calculateLogLikelihood(test_data);
	learnedModel.size = model.getSize();
	learnedModel.nb_iterations = iteration;
	learnedModel.cpu_time = time;
	learnedModel.features = model.getFeaturesString();


	if(debug_l || debug_L)
		cout << model_to_string(learnedModel) << endl;

	log_write(model_to_string(learnedModel));

	bestModel.free();

	return learnedModel;



}


LearnedModel learnModel(int nbVars, string training_data_path, string validation_data_path, string test_data_path, FeatureGenerator* fg, double alpha, bool keepOnLearning){
	return learnModel(nbVars, training_data_path, validation_data_path, test_data_path, fg, alpha,keepOnLearning,"");
}

LearnedModel learnModelAuto(int nbVars, string training_data_path, string validation_data_path, string test_data_path, FeatureGenerator* fg, double alpha, bool keepOnLearning){
	return learnModelAuto(nbVars, training_data_path,validation_data_path,test_data_path,fg,alpha,keepOnLearning,"");
}

LearnedModel learnModelMinimize(int nbVars, string training_data_path, string validation_data_path, string test_data_path, FeatureGenerator* fg, double alpha, bool keepOnLearning){
	return learnModelMinimize(nbVars, training_data_path,validation_data_path,test_data_path,fg,alpha,keepOnLearning,"");
}


LearnedModel learnModel(int nbVars, string training_data_path, string validation_data_path, string test_data_path, FeatureGenerator* fg, double alpha, bool keepOnLearning, string plotfile){
	return learnModel(nbVars, training_data_path, validation_data_path, test_data_path, fg, alpha, false,false, keepOnLearning,plotfile);
}

LearnedModel learnModelAuto(int nbVars, string training_data_path, string validation_data_path, string test_data_path, FeatureGenerator* fg, double alpha, bool keepOnLearning, string plotfile){
	return learnModel(nbVars, training_data_path,validation_data_path,test_data_path,fg,alpha,true,false, keepOnLearning,plotfile);
}

LearnedModel learnModelMinimize(int nbVars, string training_data_path, string validation_data_path, string test_data_path, FeatureGenerator* fg, double alpha, bool keepOnLearning, string plotfile){
	return learnModel(nbVars, training_data_path,validation_data_path,test_data_path,fg,alpha,false,true, keepOnLearning,plotfile);
}

string model_to_string(LearnedModel model){
	string str = "LEARNED MODEL: \n";
	str+= "LL: " + to_string(model.likelihood) + "\n";
	str+= "Size: " + to_string(model.size) + "\n";
	str+= "Nb Iterations: " + to_string(model.nb_iterations) + "\n";
	str+= "CPU time: " + to_string(model.cpu_time) + "\n";
	str+= "Features: " + model.features;
	return str;
}

void plot_write_new(double time, double LL, int size, const string &path){
    std::ofstream file(
    		path, std::ios_base::out);
    file << to_string(time)+";"+to_string(LL)+";"+to_string(size) << std::endl;
}

void plot_write(double time, double LL, int size, const string &path){
    std::ofstream file(
    		path, std::ios_base::out | std::ios_base::app );
    file << to_string(time)+";"+to_string(LL) +";"+to_string(size) << std::endl;
}

string to_string(LearnedModel model , string sep){
	return to_string(model.likelihood)+sep+
			to_string(model.size)+sep+
			to_string(model.nb_iterations)+sep+
			to_string(model.cpu_time)+sep+
			model.features;
}
