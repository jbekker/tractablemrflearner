/*
 * SimpleClusterFeatureGenerator.cpp
 *
 *  Created on: 28 Mar 2014
 *      Author: jessa
 */

#include "FeatureGenerator.h"

SimpleClusterFeatureGenerator::~SimpleClusterFeatureGenerator() {

}

bool simple_equals(const FeatureElements& first, FeatureElements& second){
	return (first.result==second.result && first.criteria==second.criteria) ||
			(first.result == ClusterFeatureGenerator::to_int(second.criteria) &&
					second.result == ClusterFeatureGenerator::to_int(first.criteria));
}

void SimpleClusterFeatureGenerator::removeDuplicates(){
	m_possibilities.unique(simple_equals);
}


Term SimpleClusterFeatureGenerator::featureElementsToTerm(FeatureElements fe){
	Term term = Term::conjoin(Term(to_int(fe.criteria)),Term(fe.result));
	term.setDependencies(fe.dependencies);
	return term;
}

void SimpleClusterFeatureGenerator::initializeFirst(Model model){
	vector<vector<bool>> feature_presence = model.getTrainingData().getFeaturesPresence();

	int S = feature_presence[0].size();
	int F = feature_presence.size();

	m_nb_true = vector<int>(F,0);
	vector<int> pair_true = vector<int>((F-1)*(F-2)/2,0);//workaround
	//	vector<int> pair_true = vector<int>((F-2)*(F-3)/2,0);//workaround

	for(int s=0 ; s<S ; s++){
		int i = 0;
		for(int f1=1 ; f1<F ; f1++){ //workaround
			//		for(unsigned int f1=2 ; f1<F ; f1++){ //workaround
			if(feature_presence[f1][s]){
				m_nb_true[f1]++;

				for(int f2=f1+1 ; f2<F ; f2++){
					pair_true[i]+=feature_presence[f2][s];
					i++;

				}
			}
			else{
				i+=F-f1-1;
			}
		}
	}


	int i = 0;
	for(int f1=1 ; f1<F ; f1++){ //workaround
		//	for(int f1=2 ; f1<F ; f1++){ //workaround
		for(int f2=f1+1 ; f2<F ; f2++){
			add_possibilities_if_interesting(S,f1,f2,pair_true[i],m_nb_true[f1],m_nb_true[f2],model);
			i++;
		}
	}
}
/*
void SimpleClusterFeatureGenerator::add_possibilities_if_interesting(int S, int f1,int f2,int pp,int np,int pn,int nn, set<int> f1_dep, set<int> f2_dep){
	int p1 = pp+pn;
	int n1 = np+nn;
	int p2 = pp+np;
	int n2 = pn+nn;

	if(f1_dep.find(f2)!=f1_dep.end())
		return;
	if(f2_dep.find(f1)!=f2_dep.end())
		return;

	set<int> dependencies = set<int>(f1_dep);
	dependencies.insert(f2_dep.begin(),f2_dep.end());
	dependencies.insert(f1);
	dependencies.insert(f2);



	addPossibilityIfInteresting(f1,f2,S,nb_12,nb_1,nb_2,set<int>(),dependencies);
	addPossibilityIfInteresting(f2,f1,S,nb_12,nb_1,nb_2,set<int>(),dependencies);

	double i_pp = false;
	double i_pn = false;
	double i_np = false;
	double i_nn = false;

	// split op f1, evalueer op f2
	double p_before_0 = double(m_nb_true[f2])/S;
	double p_after1_0 = double(pp)/p1;
	double p_after2_0 = double(np)/n1;
	double fraction = double(m_nb_true[f1])/S;
	double infoGain = calcInfoGain(p_before_0, p_after1_0, p_after2_0,fraction);
	if(infoGain>m_treshold){
		if(p_before_0>=0.5){
			if(p_after1_0<0.5)
				i_pn = true;
			else if(p_after1_0>p_before_0)
				i_pp = true;
			if(p_after2_0<0.5)
				i_nn = true;
			else if(p_after2_0>p_before_0)
				i_np = true;
		}
		else{
			if(p_after1_0>0.5)
				i_pp = true;
			else if(p_after1_0<p_before_0)
				i_pn = true;
			if(p_after2_0>0.5)
				i_np = true;
			else if(p_after2_0<p_before_0)
				i_nn = true;
		}
	}

	// split op f2, evalueer op f1 //nodig? ja!
	p_before_0 = double(m_nb_true[f1])/S;
	p_after1_0 = double(pp)/p2;
	p_after2_0 = double(pn)/n2;
	fraction = double(m_nb_true[f2])/S;
	infoGain = calcInfoGain(p_before_0, p_after1_0, p_after2_0,fraction);
	if(infoGain>m_treshold){
		if(p_before_0>=0.5){
			if(p_after1_0<0.5)
				i_np = true;
			else if(p_after1_0>p_before_0)
				i_pp = true;
			if(p_after2_0<0.5)
				i_nn = true;
			else if(p_after2_0>p_before_0)
				i_pn = true;
		}
		else{
			if(p_after1_0>0.5)
				i_pp = true;
			else if(p_after1_0<p_before_0)
				i_np = true;
			if(p_after2_0>0.5)
				i_pn = true;
			else if(p_after2_0<p_before_0)
				i_nn = true;
		}
	}

	if(i_pp)
		addPossibility( f1, f2,dependencies,infoGain);
	if(i_pn)
		addPossibility( f1,-f2,dependencies,infoGain);
	if(i_np)
		addPossibility(-f1, f2,dependencies,infoGain);
	if(i_nn)
		addPossibility(-f1,-f2,dependencies,infoGain);
}
*/
void SimpleClusterFeatureGenerator::addPossibility(int result, set<int> criteria, set<int> dependencies, double score){
	// make sure that f1 is smaller than f2
	if (result>to_int(criteria)){
		int tmp = result;
		result=to_int(criteria);
		criteria=set<int>();
		criteria.insert(tmp);
	}

	ClusterFeatureGenerator::addPossibility(result,criteria,dependencies,score);
}

void SimpleClusterFeatureGenerator::extendPossibilities(Model model){
	vector<vector<bool>> feature_presence = model.getTrainingData().getFeaturesPresence();

	int S = feature_presence[0].size();
	int F = feature_presence.size();
	int first_new_f = m_nb_true.size();
	if(first_new_f == F)
		return;

	vector<int> pair_true;

	for(int s=0 ; s<S ; s++){
		int i = 0;
		for(int f2=first_new_f ; f2<F ; f2++){
			if(s==0)
				m_nb_true.push_back(0);
			if(feature_presence[f2][s]){
				m_nb_true[f2]++;

				for(unsigned int f1=1 ; f1<f2 ; f1++){ // workaround
					//				for(unsigned int f1=2 ; f1<f2 ; f1++){ // workaround
					if(s==0)
						pair_true.push_back(0);
					pair_true[i] += feature_presence[f1][s];

					i++;
				}
			}
			else{
				i+=f2-1;
				if(s==0){
					for(unsigned int f1=1 ; f1<f2 ; f1++){
						pair_true.push_back(0);
					}
				}
			}
		}
	}


	int i = 0;
	for(int f2=first_new_f ; f2<F ; f2++){
		for(int f1=1 ; f1<f2 ; f1++){ // workaround
			//		for(int f1=2 ; f1<f2 ; f1++){ // workaround

			int pp = pair_true[i];
			int pn = m_nb_true[f1]-pp;
			int np = m_nb_true[f2]-pp;
			int nn = S - (pp + np + pn);

			add_possibilities_if_interesting(S,f1,f2,pair_true[i],m_nb_true[f1],m_nb_true[f2],model);
			i++;
		}
	}
}

void SimpleClusterFeatureGenerator::printFeatureElements(FeatureElements fe){
	cout << fe.score << " ; " << to_int(fe.criteria) << " & " << fe.result << " ;";
	for(int d : fe.dependencies){
		cout << " " << d;
	}
	cout << endl;
}
