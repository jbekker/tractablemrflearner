/*
 * JavaFeatureGenerator.cpp
 *
 *  Created on: 4 May 2014
 *      Author: jessa
 */

#include "FeatureGenerator.h"

JavaFeatureGenerator::JavaFeatureGenerator(string treeType, string featureGenerationMethod):
m_treeType(treeType), m_featureGenerationMethod(featureGenerationMethod){
	string filename  = to_string(rand());
	m_arffPath = "relation" + filename+".arff";
	m_featuresPath = "features" + filename + ".txt";
}

bool JavaFeatureGenerator::hasNext(const Model model){
	return (m_next_feature!=m_features.end());
}

Term JavaFeatureGenerator::next(const Model model){
	Term next = featureToTerm(*m_next_feature);
	m_next_feature++;
	return next;

}

void JavaFeatureGenerator::mark(){
	m_marked = m_next_feature;
	m_marked--;
}

Term JavaFeatureGenerator::featureToTerm(vector<int> feature){
	vector<Term> terms;
	for(int a : feature){
		terms.push_back(Term(a));
	}
	return Term::conjoin(terms);
}

void JavaFeatureGenerator::generateFeatures(const Model model){
	model.getTrainingData().saveAsArff(m_arffPath, "relation");
	system(("java -jar FeatureLearning.jar "+ m_arffPath + " " + m_featuresPath +" " + m_treeType + " " + m_featureGenerationMethod).c_str());
	readInFeatures();
	system(("rm " + m_featuresPath).c_str());
	system(("rm " + m_arffPath).c_str());
}

void JavaFeatureGenerator::readInFeatures(){
	m_features = vector<vector<int>>();
	string line;
	ifstream text (m_featuresPath);

	if (text.is_open()){
		while (text.good()){
			getline(text,line);
			if(line!=""){
				vector<int> feature;
				int attr;
				stringstream ss(line);
				while (ss >> attr){
					feature.push_back(attr);
				}
				m_features.push_back(feature);

			}
		}
		text.close();
	}
	else{
		std::cout << "Unable to open file " << m_featuresPath << std::endl << std::endl;
	}
}

void JavaFeatureGeneratorWithRegeneration::initialize(const Model model){
	generateFeatures(model); //somehow remove features that were already added?
	m_next_feature = m_features.begin();
}

void JavaFeatureGeneratorWithoutRegeneration::initialize(const Model model){
	if(m_first)
		generateFeatures(model);
	else{
		m_features.erase(m_marked);
	}
	m_next_feature = m_features.begin();
	m_first=false;
}


void JavaFeatureGenerator::printPossibilities(){
	for(vector<int> feature : m_features){
		for(int attr : feature){
			cout << attr << " ";
		}
		cout << endl;
	}
}
