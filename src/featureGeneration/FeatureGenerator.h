/*
 * Rule.h
 *
 *  Created on: 11 Mar 2014
 *      Author: jessa
 */

#ifndef FG_H_
#define FG_H_

#include <vector>
#include <set>
#include <cmath>
#include <functional>
#include <algorithm>
#include <list>

#include "../Term/Term.h"
#include "../Model/Model.h"

using namespace std;

/*
 * For correct functioning:
 * Always start with initialize
 * Always call hasNext exactly once before next.
 * hasNext() generates the next one, you can retrieve it with next().
 *   Not called hasNext() -> Next() gives same feature as in previous call
 *   Twice called hasNext() -> Next() skips a feature
 */
class FeatureGenerator {
  public:
	virtual ~FeatureGenerator(){}
	virtual void initialize(const Model model) = 0;
	virtual bool hasNext(const Model model) = 0;
	virtual Term next(const Model model) = 0;
	virtual void mark() = 0;
	virtual string toString(string sep) = 0 ;

};

class JavaFeatureGenerator: public FeatureGenerator{
protected:
	string m_arffPath;
	string m_featuresPath;
	string m_treeType;
	string m_featureGenerationMethod;

	vector<vector<int>> m_features;
	vector<vector<int>>::iterator m_next_feature;
	vector<vector<int>>::iterator m_marked;

	Term featureToTerm(vector<int> feature);

	void generateFeatures(const Model model);
	void readInFeatures();

public:
		JavaFeatureGenerator(string treeType, string featureGenerationMethod);

		virtual void initialize(const Model model) = 0;
		bool hasNext(const Model model);
		Term next(const Model model);
		void mark();

		void printPossibilities();
		virtual string toString(string sep) = 0 ;
};

class JavaFeatureGeneratorWithRegeneration: public JavaFeatureGenerator{
public:
		JavaFeatureGeneratorWithRegeneration(string treeType, string featureGenerationMethod):
			JavaFeatureGenerator(treeType,featureGenerationMethod){}
		void initialize(const Model model);

		string toString(string sep) {return "java"+sep+m_treeType+"_"+m_featureGenerationMethod + sep+ "regeneration";}
};

class JavaFeatureGeneratorWithoutRegeneration: public JavaFeatureGenerator{
protected:
	bool m_first;
public:
	JavaFeatureGeneratorWithoutRegeneration(string treeType, string featureGenerationMethod):
		JavaFeatureGenerator(treeType,featureGenerationMethod), m_first(true) {}
		void initialize(const Model model);

		string toString(string sep) {return "java"+sep+m_treeType+sep+m_featureGenerationMethod + sep + "noRegeneration";}
};


class BaselineFeatureGenerator: public FeatureGenerator {
private:
	bool has_next;
	Term m_next;
	int currentVar1;
	int currentVar2;
	bool currentPos1;
	bool currentPos2;

	int max_var;

	Term createTermFromSetting(const Model model);
	bool nextAlreadyExists(const Model model);
	void moveToNextSetting();

public:
	BaselineFeatureGenerator();
	virtual ~BaselineFeatureGenerator();

	virtual void initialize(const Model model);
	virtual bool hasNext(const Model model);
	virtual Term next(const Model model);
	virtual void mark(){}

	string toString(string sep) {return "baseline"+sep+sep+sep;}

};



struct FeatureElements{
	set<int> criteria;
	int result;
	set<int> dependencies;
	double score;
};

class ClusterFeatureGenerator: public FeatureGenerator{
protected:
	bool first;

	list<FeatureElements>::iterator m_next_element;
	list<FeatureElements>::iterator m_max_element;

	double m_treshold;
	int m_max_features;

	list<FeatureElements> m_possibilities;
	list<FeatureElements>::iterator m_marked;

	vector<int> m_nb_true;

	virtual void initializeFirst(const Model model) = 0;
	virtual void extendPossibilities(const Model model) = 0;

	virtual void sortPossibilities();
	virtual void removeDuplicates();
	virtual Term featureElementsToTerm(FeatureElements fe) = 0;

	virtual void addPossibility(int result, set<int> criteria, set<int> dependencies, double score);
	void addPossibilityIfInteresting(int f_result, int f_criterium, int nbSamples, int nb_12, int nb_1, int nb_2, set<int> previousCriteria, set<int> dependencies);
	void add_possibilities_if_interesting(int nbSamples, int f1,int f2, int nb_12, int nb_1, int nb_2, const Model model);

	set<int> generateDependencies(FeatureElements* fe, int newFeature, const Model model);
	set<int> generateDependencies(int f1, int f2, const Model model);
	set<int> addCriterium(int newCriterium, set<int> previousCriteria);

	bool sampleSatisfiesCriteria(int sample,set<int> criteria, vector<vector<bool>>& feature_presence);

	double calcInfoGain(double p_before_0, double p_after1_0, double p_after2_0, double fraction);
	double calcEntropy(double p0);


public:
	ClusterFeatureGenerator(double treshold, int max_features):
		first(true),
		m_treshold(treshold),
		m_max_features(max_features){}

	void initialize(const Model model);

	bool hasNext(const Model model);
	Term next(const Model model);
	void mark();

	static int to_int(set<int> s);

	void printPossibilities();
	virtual void printFeatureElements(FeatureElements fe) = 0;

	virtual string toString(string sep) = 0;

};

class SimpleClusterFeatureGenerator: public ClusterFeatureGenerator {
protected:

	virtual void removeDuplicates();
	Term featureElementsToTerm(FeatureElements fe);

	void initializeFirst(const Model model);
	void extendPossibilities(const Model model);

//	void add_possibilities_if_interesting(int S, int f1,int f2,int pp,int np,int pn,int nn,const set<int> f1_dep, const set<int> f2_dep);
	virtual void addPossibility(int result, set<int> criteria, set<int> dependencies, double score);

public:
	SimpleClusterFeatureGenerator(double treshold, int max_features): ClusterFeatureGenerator(treshold,max_features){}

	virtual ~SimpleClusterFeatureGenerator();

	void printFeatureElements(FeatureElements fe);

	string toString(string sep) {return "cluster"+ sep+ "simple" + sep +to_string(m_treshold)+sep+to_string(m_max_features);}


};



class HierarchicClusterFeatureGenerator: public ClusterFeatureGenerator {
protected:
	Term featureElementsToTerm(FeatureElements fe);

	void initializeFirst(const Model model);
	void extendPossibilities(const Model model);
	void countOccurences(vector<int>& nb_true, vector<int>& pair_true, int S, int F, vector<vector<bool>>& feature_presence);
	void addLevel(list<FeatureElements>::iterator start, list<FeatureElements>::iterator stop, set<int> const features, vector<vector<bool>>& feature_presence, int S, const Model model);

public:
	HierarchicClusterFeatureGenerator(double treshold, int max_features): ClusterFeatureGenerator(treshold,max_features){}

	virtual ~HierarchicClusterFeatureGenerator();

	void printFeatureElements(FeatureElements fe);

	string toString(string sep) {return "cluster"+ sep + "hierarchic"+ sep +to_string(m_treshold)+sep+to_string(m_max_features);}


};




void eraseAll(set<int>* s, set<int>* toBeErased);

#endif /* FG_H_ */
