/*
 * BaselineFeatureGenerator.cpp
 *
 *  Created on: 28 Mar 2014
 *      Author: jessa
 */

#include "FeatureGenerator.h"

BaselineFeatureGenerator::BaselineFeatureGenerator() {
}

BaselineFeatureGenerator::~BaselineFeatureGenerator() {

}


void BaselineFeatureGenerator::initialize(const Model model){
	max_var = model.getNbVars();

	currentVar1 = 0; //workaround
//	currentVar1 = 1; //workaround
	currentVar2 = max_var;
	currentPos1=false;
	currentPos2=false;
}

bool BaselineFeatureGenerator::hasNext(const Model model){

	moveToNextSetting();
	if(currentVar1>=max_var)
		return false;
	m_next = createTermFromSetting(model);
	if(nextAlreadyExists(model))
		return hasNext(model);
	else
		return true;
}

Term BaselineFeatureGenerator::next(const Model model){
	m_next.deref();
	return m_next;
}

Term BaselineFeatureGenerator::createTermFromSetting(const Model model){
	Term v1 = Term(currentVar1);
	Term v2 = Term(currentVar2);
	v1.ref();
	v2.ref();
	if(!currentPos1){
		v1.deref();
		v1 = Term::neg(v1);
		v1.ref();
	}
	if(!currentPos2){
		v2.deref();
		v2 = Term::neg(v2);
		v2.ref();
	}
	v1.deref();
	v2.deref();
	Term term = Term::conjoin(v1,v2);
	term.ref();
	return term;
}

bool BaselineFeatureGenerator::nextAlreadyExists(const Model model){
	for(int i=model.getNbVars()+1; i<=model.getNbFeatures() ; i++){
		if(m_next.equals(model.getFeature(i))){
			return true;
			m_next.deref();
		}
	}
	return false;
}

void BaselineFeatureGenerator::moveToNextSetting(){
	if(currentPos2){
		currentPos2=false;
		return;
	}
	else if(currentPos1){
		currentPos1=false;
		currentPos2=true;
		return;
	}
	else{
		currentPos1=true;
		currentPos2=true;

		if(currentVar2 == max_var){
			currentVar1++;
			currentVar2 = currentVar1+1;
			return;
		}
		else{
			currentVar2++;
		}
	}
}
