

#include "FeatureGenerator.h"

bool greater_than(const FeatureElements& first, FeatureElements& second){
	if (first.score == second.score){
		if(first.result == second.result){
			return first.criteria>second.criteria;
		}
		else
			return first.result>second.result;
	}
	else
		return first.score>second.score;
}


bool equals(const FeatureElements& first, FeatureElements& second){
	return first.result==second.result && first.criteria==second.criteria;
}


int ClusterFeatureGenerator::to_int(set<int> s){
	return *s.begin();
}


void ClusterFeatureGenerator::initialize(const Model model){

	if(first){
		initializeFirst(model);
		first = false;
	}
	else{
		m_possibilities.erase(m_marked);
		extendPossibilities(model);
	}

	sortPossibilities();
	removeDuplicates();

	m_next_element = m_possibilities.begin();
	if (m_max_features>= m_possibilities.size())
		m_max_element = m_possibilities.end();
	else{
		m_max_element = m_possibilities.begin();
		for(int i =0; i<m_max_features; i++)
			m_max_element++;
	}
}

void ClusterFeatureGenerator::sortPossibilities(){
	m_possibilities.sort(greater_than);
}

void ClusterFeatureGenerator::removeDuplicates(){
	m_possibilities.unique(equals);
}

bool ClusterFeatureGenerator::hasNext(const Model model){
	return (m_next_element!=m_possibilities.end() && m_next_element!=m_max_element);
}

Term ClusterFeatureGenerator::next(const Model model){
	Term next = featureElementsToTerm(*m_next_element);
	m_next_element++;
	return next;
}

void ClusterFeatureGenerator::mark(){
	m_marked = m_next_element;
	m_marked--;
}


void ClusterFeatureGenerator::add_possibilities_if_interesting(int nbSamples, int f1,int f2, int nb_12, int nb_1, int nb_2, const Model model){
	set<int> dep_1 = model.getDependencies(f1);
	set<int> dep_2 = model.getDependencies(f2);
	if(dep_1.find(f2)!=dep_1.end())
		return;
	if(dep_2.find(f1)!=dep_2.end())
		return;

	set<int> dependencies = generateDependencies(f1,f2,model);

	addPossibilityIfInteresting(f1,f2,nbSamples,nb_12,nb_1,nb_2,set<int>(),dependencies);
	addPossibilityIfInteresting(f2,f1,nbSamples,nb_12,nb_1,nb_2,set<int>(),dependencies);

}

/*
 * pp : P(result = 1 & criterium = 1)
 * np : P(result = 0 & criterium = 1)
 * pn : P(result = 1 & criterium = 0)
 * nn : P(result = 0 & criterium = 0)
 */
void ClusterFeatureGenerator::addPossibilityIfInteresting(int f_result, int f_criterium, int nbSamples, int nb_12, int nb_1, int nb_2, set<int> previousCriteria, set<int> dependencies){

	int pp = nb_12;
	int pn = nb_1-pp;
	int np = nb_2-pp;
	int nn = nbSamples - (pp + np + pn);

	int p_result = pp+pn;
	int p_crit = pp+np;
	int n_crit = pn+nn;
	if(p_crit==0 || n_crit==0)
		return;

	// split op f_criterium, evalueer op f_result //nodig? ja!
	double p_before = double(p_result)/nbSamples;
	double p_crit_p = double(pp)/p_crit;
	double p_crit_n = double(pn)/n_crit;
	double fraction = double(p_crit)/nbSamples;
	if (f_result==3 && f_criterium == 4){
		int d=1;
	}
	double infoGain = calcInfoGain(p_before, p_crit_p, p_crit_n,fraction);
	if(infoGain>m_treshold){
		if(p_before>=0.5){
			if(p_crit_p<0.5)
				addPossibility(-f_result,addCriterium(f_criterium,previousCriteria),dependencies,infoGain);
			else if(p_crit_p>p_before)
				addPossibility(f_result,addCriterium(f_criterium,previousCriteria),dependencies,infoGain);
			if(p_crit_n<0.5)
				addPossibility(-f_result,addCriterium(-f_criterium,previousCriteria),dependencies,infoGain);
			else if(p_crit_n>p_before)
				addPossibility(f_result,addCriterium(-f_criterium,previousCriteria),dependencies,infoGain);
		}
		else{
			if(p_crit_p>0.5)
				addPossibility(f_result,addCriterium(f_criterium,previousCriteria),dependencies,infoGain);
			else if(p_crit_p<p_before)
				addPossibility(-f_result,addCriterium(f_criterium,previousCriteria),dependencies,infoGain);
			if(p_crit_n>0.5)
				addPossibility(f_result,addCriterium(-f_criterium,previousCriteria),dependencies,infoGain);
			else if(p_crit_n<p_before)
				addPossibility(-f_result,addCriterium(-f_criterium,previousCriteria),dependencies,infoGain);
		}
	}

}


void ClusterFeatureGenerator::addPossibility(int result, set<int> criteria, set<int> dependencies, double score){
	FeatureElements fe;
	fe.criteria = criteria;
	fe.result = result;
	fe.dependencies = dependencies;
	fe.score = score;
	m_possibilities.push_back(fe);
}


double ClusterFeatureGenerator::calcInfoGain(double p_before_0, double p_after1_0, double p_after2_0, double fraction){
	double S_before = calcEntropy(p_before_0);
	double S_after = fraction*calcEntropy(p_after1_0)+(1-fraction)*calcEntropy(p_after2_0);
	return S_before-S_after;
}

double ClusterFeatureGenerator::calcEntropy(double p0){
	if(p0==0 || p0==1)
		return 0;
	double p1 = 1-p0;
	return -(p0*log2(p0)+p1*log2(p1));
}


set<int> ClusterFeatureGenerator::generateDependencies(FeatureElements* fe, int newFeature, const Model model){
	set<int> dependencies = set<int>(fe->dependencies);
	dependencies.insert(newFeature);
	set<int> dependencies_f = model.getDependencies(newFeature);
	dependencies.insert(dependencies_f.begin(),dependencies_f.end());
	return dependencies;
}

set<int> ClusterFeatureGenerator::generateDependencies(int f1, int f2, const Model model) {
	set<int> dependencies = set<int>(model.getDependencies(f1));
	set<int> dep_2 = model.getDependencies(f2);
	dependencies.insert(dep_2.begin(),dep_2.end());
	dependencies.insert(f1);
	dependencies.insert(f2);
	return dependencies;
}

bool ClusterFeatureGenerator::sampleSatisfiesCriteria(int sample,set<int> criteria, vector<vector<bool>>& feature_presence){
	for(int criterium : criteria)
		if(feature_presence[abs(criterium)][sample]){
			if(criterium<0)
				return false;
		}
		else
			if(criterium>0)
				return false;

	return true;
}

set<int> ClusterFeatureGenerator::addCriterium(int newCriterium, set<int> previousCriteria){
	previousCriteria.insert(newCriterium);
	return previousCriteria;
}


void ClusterFeatureGenerator::printPossibilities(){
	cout << m_possibilities.size() << endl;
	for(auto fe = m_possibilities.begin() ; fe!=m_max_element ; fe++)
		printFeatureElements(*fe);
}

void eraseAll(set<int>* s, set<int>* toBeErased){
	for (set<int>::iterator it=toBeErased->cbegin() ; it!=toBeErased->cend() ; it++)
					s->erase(*it);
}

