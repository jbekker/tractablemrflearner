/*
 * HierarchicClusterFeatureGenerator.cpp
 *
 *  Created on: 28 Mar 2014
 *      Author: jessa
 */

#include "FeatureGenerator.h"


HierarchicClusterFeatureGenerator::~HierarchicClusterFeatureGenerator(){

}


Term HierarchicClusterFeatureGenerator::featureElementsToTerm(FeatureElements fe){
	vector<Term> conj_elements;
	for(int c : fe.criteria){
		Term t = Term(c);
		conj_elements.push_back(t);
	}

	Term lhs = Term::conjoin(conj_elements);
	lhs.ref();
	Term term = Term::imply(lhs,Term(fe.result));
	lhs.deref();
	term.setDependencies(fe.dependencies);
	return term;
}


void HierarchicClusterFeatureGenerator::initializeFirst(const Model model){
	vector<vector<bool>> feature_presence = model.getTrainingData().getFeaturesPresence();

	int S = feature_presence[0].size();
	int F = feature_presence.size();


	m_nb_true = vector<int>(F,0);
	vector<int> pair_true = vector<int>((F-1)*(F-2)/2,0);//workaround
	//	vector<int> pair_true = vector<int>((F-2)*(F-3)/2,0);//workaround

	countOccurences(m_nb_true, pair_true,S,F,feature_presence);

	int i = 0;
	for(int f1=1 ; f1<F ; f1++){ //workaround
		//	for(int f1=2 ; f1<F ; f1++){ //workaround
		for(int f2=f1+1 ; f2<F ; f2++){ // workaround
			//		for(int f2=2 ; f2<F ; f2++){ // workaround
			if (f1!=f2){
				add_possibilities_if_interesting(S, f1,f2,pair_true[i],m_nb_true[f1],m_nb_true[f2],model);
			}
			i++;
		}
	}



	list<FeatureElements>::iterator start = m_possibilities.begin();
	list<FeatureElements>::iterator stop  = m_possibilities.end();

	set<int> features;
	for(int i=2 ; i < F ; i++ )
		features.insert(i);

	while(start!=stop){
		addLevel(start,stop,features,feature_presence,S,model);
		start = stop;
		stop  = m_possibilities.end();
	}

}

void HierarchicClusterFeatureGenerator::countOccurences(vector<int>& nb_true, vector<int>& pair_true, int S, int F, vector<vector<bool>>& feature_presence){
	for(int s=0 ; s<S ; s++){
		int i = 0;
		for(int f1=1 ; f1<F ; f1++){ //workaround
			//		for(int f1=2 ; f1<F ; f1++){ //workaround
			if(feature_presence[f1][s]){
				nb_true[f1]++;

				for(int f2=f1+1; f2<F ; f2++){ // workaround
					//				for(int f2=2; f2<F ; f2++){ // workaround
					pair_true[i]+=feature_presence[f2][s];
					i++;

				}
			}
			else{
				i+=F-f1-1;
			}
		}
	}
}

void HierarchicClusterFeatureGenerator::addLevel(list<FeatureElements>::iterator start, list<FeatureElements>::iterator stop, set<int> const features, vector<vector<bool>>& feature_presence, int S, const Model model){
	for(list<FeatureElements>::iterator fe = start ; fe != stop ; fe++){

//		printFeatureElements(*fe);

		set<int> pos = set<int>(features);
		eraseAll(&pos, &(fe->dependencies));

		vector<int> nb_true  = vector<int>(pos.size(),0);
		vector<int> nb_pairs = vector<int>(pos.size(),0);
		int nb_result = 0;
		int nbSamples = 0;

		for (int s=0 ; s<S ; s++){
			if(sampleSatisfiesCriteria(s,fe->criteria, feature_presence)){
				nbSamples++;
				bool resultSatisfied;
				if(fe->result>0)
					resultSatisfied = feature_presence[abs(fe->result)][s];
				else
					resultSatisfied = !feature_presence[abs(fe->result)][s];
				if(resultSatisfied)
					nb_result++;
				int i=0;
				for (int f : pos){
					if(feature_presence[f][s]){
						nb_true[i]++;
						if(resultSatisfied)
							nb_pairs[i]++;
					}
					i++;
				}
			}
		}

		int i=0;
		for (int f : pos){

			addPossibilityIfInteresting(fe->result,f, nbSamples,nb_pairs[i],nb_result,nb_true[i],fe->criteria,generateDependencies(&(*fe),f,model));
			//			cout <<f << "->" << m_possibilities.size() << endl;

			i++;
		}

	}
}

void HierarchicClusterFeatureGenerator::extendPossibilities(const Model model){
	vector<vector<bool>> feature_presence = model.getTrainingData().getFeaturesPresence();

	int S = feature_presence[0].size();
	int nb_features_in_data = feature_presence.size();


	list<FeatureElements>::iterator start = m_possibilities.end();

	while(m_nb_true.size()<nb_features_in_data){
		int f = m_nb_true.size();
		m_nb_true.push_back(0);

		vector<int> pair_true = vector<int>(f,0);//workaround

		for(int s=0 ; s<S ; s++){
			if(feature_presence[f][s]){
				m_nb_true[f]++;
				for(int f2=1 ; f2<f ; f2++){ //workaround
//				for(int f2=2 ; f2<F ; f2++){ //workaround
					pair_true[f2]+=feature_presence[f2][s];
				}
			}
		}

		for(int f2=1 ; f2<f ; f2++){ //workaround
//		for(int f2=2 ; f2<F ; f2++){ //workaround
			add_possibilities_if_interesting(S, f,f2,pair_true[f2],m_nb_true[f],m_nb_true[f2],model);
		}


		list<FeatureElements>::iterator stop  = m_possibilities.end();

		set<int> features;
		for(int i=2 ; i < f ; i++ )
			features.insert(i);

		while(start!=stop){
			addLevel(start,stop,features,feature_presence,S,model);
			start = stop;
			stop  = m_possibilities.end();
		}
	}
}


void HierarchicClusterFeatureGenerator::printFeatureElements(FeatureElements fe){
	cout << fe.score << " ; " << fe.result << " <= " ;
	for(int c : fe.criteria){
		cout << " " << c;
	}
	cout << " ;" ;
	for(int d : fe.dependencies){
		cout << " " << d;
	}
	cout << endl;
}

bool operator==(const FeatureElements& lhs, const FeatureElements& rhs)
				{
	return (lhs.criteria==rhs.criteria && lhs.result == rhs.result);
				}
