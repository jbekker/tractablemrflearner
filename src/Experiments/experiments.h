/*
 * experiments.h
 *
 *  Created on: 7 Apr 2014
 *      Author: jessa
 */

#ifndef EXPERIMENTS_H_
#define EXPERIMENTS_H_

#include "../learner.h"
#include "../Data/Data.h"

#include <cmath>
#include <vector>
#include <string>
#include <sstream>
#include <iostream>
#include <fstream>
#include <set>
#include <map>

using namespace std;

const string resultpath = "experimentResults/";

const string datapath = "data/";

const string test = ".test.data";
const string ts = ".train.data";
const string valid = ".valid.data";
const string config = ".config";

//const string datasets[12] = {
//		"nltcs", //16
//		"msnbc", //17
//		"kdd", //64
//		"plants", //69
//		"baudio", //100
//		"jester", //100
//		"bnetflix", //100
//		"accidents", //111
//		"tretail", //135
//		"pumsb_star", //163
//		"dna", //180
//		"msweb", //294
//		"book", //500
//		"tmovie", //
//		"cwebkb", //839
//		"lkosarak", //883
//		"cr52", //889
//		"c20ng", //910
//		"bbc", //330
//		"ad" //491
//};


const string datasets[1] = {
		"nltcs"
};

const int nb_vars_per_dataset[1] = {
		5
};

//const string datasets[1] = {
//		"generated1"
//};


struct Experiment {
	string dataset;
	int nbvars;
	string featureGenerator;
	double alpha;
	double treshold;
	int limit;
	string gc;
};

struct TreeExperiment {
	string dataset;
	int nbvars;
	double alpha;
	string treeType;
	string featureGenerator;
	bool useAllFeatures;
};

/*
 * ALPHA EXPERIMENTS
 */

void runAlphaExperiment();
void runAlphaExperiment(string dataset, int alpha_first, int alpha_last, bool plot);


/*
 * MINIMIZE AND GARBAGE COLLECTION EXPERIMENTS
 */

void runMinimizeExperiment(string dataset, int alpha_first, int alpha_last);
void runMinimizeExperiment(string dataset, double alpha);
void runAutoExperiment(string dataset, int alpha_first, int alpha_last);
void runGcComparationExperiment(string dataset, double alpha);


/*
 * FEATURE GENERATOR EXPERIMENTS : SIMPLE CLUSTERING
 */

void runSimpleClusteringTresholdExperiment(string dataset, double alpha, int treshold_first, int treshold_last);
void runSimpleClusteringTresholdExperiment(string dataset, double alpha, int limit, int treshold_first, int treshold_last);
void runSimpleClusteringLimitExperiment(string dataset, double alpha, int limit_first, int limit_last);
void runSimpleClusteringLimitExperiment(string dataset, double alpha, double treshold, int limit_first, int limit_last);
void runSimpleClusteringTresholdLimigExperiment(string dataset, double alpha, int treshold_first, int treshold_last, int limit_first, int limit_last);

/*
 * FEATURE GENERATOR EXPERIMENTS : TREEs
 */
void runTreeExperiment(string dataset, double alpha, int first_experimentNb, int last_experimentNb);

/*
 * HELPERS
 */

string getTestPath(string dataset);
string getTSPath(string dataset);
string getValidPath(string dataset);
string getConfigPath(string dataset);
int getNbVars(string configPath);

string toString(Experiment experiment, LearnedModel model);
string toString(TreeExperiment experiment, LearnedModel model);
Experiment toExperiment(string experiment);

#endif /* EXPERIMENTS_H_ */
