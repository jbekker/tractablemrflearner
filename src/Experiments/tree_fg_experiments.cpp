#include "experiments.h"


string treeTypes[3] = {"prunedTree", "unprunedTree", "randomTree"};
string featureGenerators[4] = {"default", "prune", "prune5", "prune10"};
bool useAllFeatures[2] = {false,true};

int experiments[7][3] = {{0,0,0},{1,0,0},{2,0,0},{0,1,0},{0,2,0},{0,3,0},{0,0,1}};

#include "experiments.h"

string TreeTest(TreeExperiment exp, string ts, string test, string valid);

void runTreeExperiment(string dataset, double alpha, int first_experimentNb, int last_experimentNb){
	string path = resultpath+"tree_"+to_string(first_experimentNb)+"_"+to_string(last_experimentNb)+".csv";

	int nb_vars = getNbVars(getConfigPath(dataset));
	string trainingData = getTSPath(dataset);
	string validData    = getValidPath(dataset);
	string testData     = getTestPath(dataset);

	ofstream myfile;
	myfile.open(path,ios::app);
	if (myfile.is_open()){
		for(int i = first_experimentNb; i<=last_experimentNb ;i++){
			TreeExperiment exp;
			exp.nbvars=nb_vars;
			exp.alpha=alpha;
			exp.dataset=dataset;
			exp.treeType=treeTypes[experiments[i][0]];
			exp.featureGenerator=featureGenerators[experiments[i][1]];
			exp.useAllFeatures=useAllFeatures[experiments[i][2]];
			myfile << TreeTest(exp,trainingData,testData,validData) << endl;
		}
	    myfile.close();
	}

	else cout << "Unable to open file " + path;
}

string TreeTest(TreeExperiment exp, string ts, string test, string valid) {
	string execute_string = "EXECUTE: dataset: " + exp.dataset  + ", alpha: " + to_string(exp.alpha)  + ", treetype: " + exp.treeType + ", featureGenerationMethod: " + exp.featureGenerator + ", use all featuers: " + to_string(exp.useAllFeatures);
	log_write(execute_string);

	FeatureGenerator* fg;
	if(exp.useAllFeatures)
		fg = new JavaFeatureGeneratorWithRegeneration(exp.treeType, exp.featureGenerator);
	else
		fg = new JavaFeatureGeneratorWithoutRegeneration(exp.treeType, exp.featureGenerator);

//	int nb_vars = exp.nbvars+1; //workaround
	int nb_vars = exp.nbvars; //workaround

	log_indent();
	LearnedModel model = learnModelMinimize(nb_vars, ts,valid,test,fg, exp.alpha,false);
	delete fg;

	log_dindent();
	log_white_line();
	log_add_line();
	log_add_line();
	log_white_line();
//	bfg.deref(g_mgr_model); //old FG
	string res = toString(exp, model);
	return res;
}
