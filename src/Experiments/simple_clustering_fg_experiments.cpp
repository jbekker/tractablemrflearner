#include "experiments.h"

using namespace std;

const double treshold_values[6] = {0.1, 0.01, 0.001, 0.0001, 0.0001, 0.00001};
const double limit_values[4] = {10, 100, 1000, 10000};

string simpleClusteringTest(Experiment exp, string ts, string test, string valid);


void runSimpleClusteringTresholdExperiment(string dataset, double alpha, int treshold_first, int treshold_last){
	runSimpleClusteringTresholdExperiment(dataset,alpha, (int) INFINITY, treshold_first, treshold_last);
}

void runSimpleClusteringTresholdExperiment(string dataset, double alpha, int limit, int treshold_first, int treshold_last){
	string path = resultpath+"simplecluster_lim_"+to_string(limit)+"_treshold"+dataset+"_"+to_string(treshold_first)+"_"+to_string(treshold_last)+".csv";

	int nb_vars = getNbVars(getConfigPath(dataset));
	string trainingData = getTSPath(dataset);
	string validData    = getValidPath(dataset);
	string testData     = getTestPath(dataset);

	ofstream myfile;
	myfile.open(path,ios::app);
	if (myfile.is_open()){
		for(int i = treshold_first; i<=treshold_last ;i++){
			Experiment exp;
			exp.nbvars=nb_vars;
			exp.alpha=alpha;
			exp.dataset=dataset;
			exp.limit=limit;
			exp.treshold=treshold_values[i];
			exp.featureGenerator = "simple_clustering";
			myfile << simpleClusteringTest(exp,trainingData,testData,validData) << endl;
		}
	    myfile.close();
	}

	else cout << "Unable to open file " + path;
}

void runSimpleClusteringLimitExperiment(string dataset, double alpha, int limit_first, int limit_last){
	runSimpleClusteringLimitExperiment(dataset,alpha,0,limit_first, limit_last);
}


void runSimpleClusteringLimitExperiment(string dataset, double alpha, double treshold, int limit_first, int limit_last){
	string path = resultpath+"simplecluster_tr_"+to_string(treshold)+"_limit"+dataset+"_"+to_string(limit_first)+"_"+to_string(limit_last)+".csv";

	int nb_vars = getNbVars(getConfigPath(dataset));
	string trainingData = getTSPath(dataset);
	string validData    = getValidPath(dataset);
	string testData     = getTestPath(dataset);

	ofstream myfile;
	myfile.open(path,ios::app);
	if (myfile.is_open()){
		for(int i = limit_first; i<=limit_last ;i++){
			Experiment exp;
			exp.nbvars=nb_vars;
			exp.alpha=alpha;
			exp.dataset=dataset;
			exp.limit=limit_values[i];
			exp.treshold=treshold;
			exp.featureGenerator = "simple_clustering";

			myfile << simpleClusteringTest(exp,trainingData,testData,validData) << endl;
		}
	    myfile.close();
	}

	else cout << "Unable to open file " + path;

}

void runSimpleClusteringTresholdLimigExperiment(string dataset, double alpha, int treshold_first, int treshold_last, int limit_first, int limit_last){
	for(int l = limit_first; l<=limit_last; l++){
		runSimpleClusteringTresholdExperiment(dataset,alpha, limit_values[l], treshold_first, treshold_last);
	}
}

string simpleClusteringTest(Experiment exp, string ts, string test, string valid) {
	string execute_string = "EXECUTE: dataset: " + exp.dataset  + ", alpha: " + to_string(exp.alpha)  + ", treshold: " + to_string(exp.treshold)  + ", limit: " + to_string(exp.limit);
	log_write(execute_string);

	SimpleClusterFeatureGenerator fg = SimpleClusterFeatureGenerator(exp.treshold,exp.limit);
//	int nb_vars = exp.nbvars+1; //workaround
	int nb_vars = exp.nbvars; //workaround

	log_indent();
	LearnedModel model = learnModel(nb_vars, ts,valid,test,&fg, exp.alpha,false);
	log_dindent();
	log_white_line();
	log_add_line();
	log_add_line();
	log_white_line();
//	bfg.deref(g_mgr_model); //old FG
	string res = toString(exp, model);
	return res;
}
