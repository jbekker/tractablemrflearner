
#include "experiments.h"

string getPath(string dataset, string type){
	return datapath + dataset + "/" + dataset + type;
}

string getTestPath(string dataset){
	return getPath(dataset, test);
}

string getTSPath(string dataset){
	return getPath(dataset, ts);
}

string getValidPath(string dataset){
	return getPath(dataset, valid);
}

string getConfigPath(string dataset){
	return getPath(dataset, config);
}

int getNbVars(string configPath){

	ifstream file ( configPath ); // declare file stream: http://www.cplusplus.com/reference/iostream/ifstream/
	if (file.is_open()){
		string line;
		getline(file, line);
		stringstream ss(line);
		int nb_vars;
		ss >> nb_vars;
		file.close();
		return nb_vars;
	}
	else{
		cout <<"unable to open config file" << endl;
		return 0;
	}


}

string toString(Experiment experiment, LearnedModel model){
	// dataset #vars featureGenerator alpha likelihood size cpu_time features
	return experiment.dataset + ";"+ to_string(experiment.nbvars) + ";"+ experiment.featureGenerator + ";"+ experiment.gc+ ";"+to_string(experiment.alpha) + ";"+ to_string(experiment.treshold) + ";"+ to_string(experiment.limit) + ";"+ to_string(model.likelihood) + ";" + to_string(model.size) + ";"+ to_string(model.nb_iterations) + ";"+ to_string(model.cpu_time) + ";" + model.features;
}

string toString(TreeExperiment experiment, LearnedModel model){
	// dataset #vars featureGenerator alpha likelihood size cpu_time features
	return experiment.dataset + ";"+ to_string(experiment.nbvars) + ";"+ experiment.treeType + ";"+ experiment.featureGenerator + ";"+ to_string(experiment.useAllFeatures) + ";"+ to_string(experiment.alpha) + ";"+ to_string(model.likelihood) + ";" + to_string(model.size) + ";"+ to_string(model.nb_iterations) + ";"+ to_string(model.cpu_time) + ";" + model.features;
}

Experiment toExperiment(string experiment) {
	Experiment exp;

	std::stringstream ss(experiment);
	string tmp;

	getline(ss, exp.dataset, ';');
	getline(ss, tmp, ';');
	exp.nbvars = stoi(tmp);
	getline(ss, exp.featureGenerator, ';');
	getline(ss, tmp, ';');
	exp.alpha = stod(tmp);

	return exp;
}


