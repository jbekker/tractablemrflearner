#include "experiments.h"


const double alpha_values[7] = {0, 0.000001,0.0001,0.01};
//const double alpha_values[1] = {0.00};

string minimize_alphatest(Experiment exp, string ts, string test, string valid);
void minimize_runAlphaExperiment(ofstream& myfile, string dataset, int alpha_first, int alpha_last);
void minimize_runAlphaExperiment(ofstream& myfile, string dataset, double alpha);
void comparation_runAlphaExperiment(ofstream& myfile, string dataset, double alpha);
string auto_alphatest(Experiment exp, string ts, string test, string valid);
string manual_alphatest(Experiment exp, string ts, string test, string valid);
void gc_runAlphaExperiment(ofstream& myfile, string dataset, int alpha_first, int alpha_last);



void runGcComparationExperiment(string dataset, double alpha) {
	string path = resultpath+"gccomp_"+dataset+".csv";


	ofstream myfile;
	myfile.open(path,ios::app);
	if (myfile.is_open()){
		comparation_runAlphaExperiment(myfile,dataset, alpha);
	    myfile.close();
	}

	else cout << "Unable to open file " + path;

}


void runMinimizeExperiment(string dataset, int alpha_first, int alpha_last){
	string path = resultpath+"minimize_"+dataset+"_"+to_string(alpha_first)+"_"+to_string(alpha_last)+".csv";

	ofstream myfile;
	myfile.open(path,ios::app);
	if (myfile.is_open()){
		minimize_runAlphaExperiment(myfile,dataset, alpha_first, alpha_last);
	    myfile.close();
	}

	else cout << "Unable to open file " + path;
}

void runMinimizeExperiment(string dataset, double alpha){
	string path = resultpath+"gccomp_"+dataset+".csv";

	ofstream myfile;
	myfile.open(path,ios::app);
	if (myfile.is_open()){
		minimize_runAlphaExperiment(myfile,dataset, alpha);
	    myfile.close();
	}

	else cout << "Unable to open file " + path;
}


void minimize_runAlphaExperiment(ofstream& myfile, string dataset, int alpha_first, int alpha_last){
	int nb_vars = getNbVars(getConfigPath(dataset));
	string trainingData = getTSPath(dataset);
	string validData    = getValidPath(dataset);
	string testData     = getTestPath(dataset);

	for(int i=alpha_first; i <=alpha_last ; i++){
		double alpha = alpha_values[i];



		string execute_string = "EXECUTE: dataset: " + dataset  + ", alpha: " + to_string(alpha);
		cout << execute_string << endl;
		log_write(execute_string);

		Experiment exp;
		exp.alpha = alpha;
		exp.dataset = dataset;
		exp.featureGenerator = "baseline";
		exp.nbvars = nb_vars;
		myfile << minimize_alphatest(exp, trainingData, testData, validData) << endl;


		sdd_manager_free(g_mgr);
	}
}

void minimize_runAlphaExperiment(ofstream& myfile, string dataset, double alpha){
	int nb_vars = getNbVars(getConfigPath(dataset));
	string trainingData = getTSPath(dataset);
	string validData    = getValidPath(dataset);
	string testData     = getTestPath(dataset);

	Experiment exp;
	exp.alpha = alpha;
	exp.dataset = dataset;
	exp.featureGenerator = "baseline";
	exp.nbvars = nb_vars;

	exp.gc = "minimize2";
	string execute_string = "EXECUTE: dataset: " + dataset  + ", alpha: " + to_string(alpha) + ", gc: "+exp.gc;
	cout << execute_string << endl;
	log_write(execute_string);

	myfile << minimize_alphatest(exp, trainingData, testData, validData) << endl;

	sdd_manager_free(g_mgr);
}

void comparation_runAlphaExperiment(ofstream& myfile, string dataset, double alpha){
	int nb_vars = getNbVars(getConfigPath(dataset));
	string trainingData = getTSPath(dataset);
	string validData    = getValidPath(dataset);
	string testData     = getTestPath(dataset);

	Experiment exp;
	exp.alpha = alpha;
	exp.dataset = dataset;
	exp.featureGenerator = "baseline";
	exp.nbvars = nb_vars;

	exp.gc = "manual";
	string execute_string = "EXECUTE: dataset: " + dataset  + ", alpha: " + to_string(alpha) + ", gc: "+exp.gc;
	cout << execute_string << endl;
	log_write(execute_string);

	myfile << manual_alphatest(exp, trainingData, testData, validData) << endl;

	sdd_manager_free(g_mgr);

	exp.gc = "minimize";
	execute_string = "EXECUTE: dataset: " + dataset  + ", alpha: " + to_string(alpha) + ", gc: "+exp.gc;
	cout << execute_string << endl;
	log_write(execute_string);

	myfile << minimize_alphatest(exp, trainingData, testData, validData) << endl;

	sdd_manager_free(g_mgr);

	exp.gc = "auto";
	execute_string = "EXECUTE: dataset: " + dataset  + ", alpha: " + to_string(alpha) + ", gc: "+exp.gc;
	cout << execute_string << endl;
	log_write(execute_string);

	myfile << auto_alphatest(exp, trainingData, testData, validData) << endl;


	sdd_manager_free(g_mgr);
}



string minimize_alphatest(Experiment exp, string ts, string test, string valid) {
	BaselineFeatureGenerator bfg;
//	int nb_vars = exp.nbvars+1; //workaround
	int nb_vars = exp.nbvars; //workaround
	string plotfile = "plot_"+ exp.dataset + "_" + exp.gc + "_" +to_string(exp.alpha)+".csv";//

	log_indent();
	LearnedModel model = learnModel(nb_vars,ts,valid,test,&bfg,exp.alpha,false,true,true,plotfile);
	log_dindent();
	log_white_line();
	log_add_line();
	log_add_line();
	log_white_line();
//	bfg.deref(g_mgr_model); //old FG
	string res = toString(exp, model);
	return res;
}

string manual_alphatest(Experiment exp, string ts, string test, string valid) {
	BaselineFeatureGenerator bfg;
//	int nb_vars = exp.nbvars+1; //workaround
	int nb_vars = exp.nbvars; //workaround
	string plotfile = "plot_"+ exp.dataset + "_" + exp.gc + "_" +to_string(exp.alpha)+".csv";

	log_indent();
	LearnedModel model = learnModel(nb_vars,ts,valid,test,&bfg,exp.alpha,false,false,true,plotfile);
	log_dindent();
	log_white_line();
	log_add_line();
	log_add_line();
	log_white_line();
//	bfg.deref(g_mgr_model); //old FG
	string res = toString(exp, model);
	return res;
}



string auto_alphatest(Experiment exp, string ts, string test, string valid) {
	BaselineFeatureGenerator bfg;
//	int nb_vars = exp.nbvars+1; //workaround
	int nb_vars = exp.nbvars; //workaround

	string plotfile = "plot_"+ exp.dataset + "_" + exp.gc + "_" +to_string(exp.alpha)+".csv";

	log_indent();
	LearnedModel model = learnModel(nb_vars,ts,valid,test,&bfg,exp.alpha,true,false,true,plotfile);
	log_dindent();
	log_white_line();
	log_add_line();
	log_add_line();
	log_white_line();
//	bfg.deref(g_mgr_model); //old FG
	string res = toString(exp, model);
	return res;
}


// AUTOMATIC GARBACE COLLECTION


void runAutoExperiment(string dataset, int alpha_first, int alpha_last){
	string path = resultpath+"atuogc"+dataset+"_"+to_string(alpha_first)+"_"+to_string(alpha_last)+".csv";

	ofstream myfile;
	myfile.open(path,ios::app);
	if (myfile.is_open()){
		gc_runAlphaExperiment(myfile,dataset, alpha_first, alpha_last);
	    myfile.close();
	}

	else cout << "Unable to open file " + path;
}


void gc_runAlphaExperiment(ofstream& myfile, string dataset, int alpha_first, int alpha_last){
	int nb_vars = getNbVars(getConfigPath(dataset));
	string trainingData = getTSPath(dataset);
	string validData    = getValidPath(dataset);
	string testData     = getTestPath(dataset);

	for(int i=alpha_first; i <=alpha_last ; i++){
		double alpha = alpha_values[i];
//		Data trainingData = Data(getTSPath(dataset),nb_vars_w,true);
//		Data validData    = Data(getValidPath(dataset),nb_vars_w,true);
//		Data testData     = Data(getTestPath(dataset),nb_vars_w,false);

		string execute_string = "EXECUTE: dataset: " + dataset  + ", alpha: " + to_string(alpha);
		cout << execute_string << endl;
		log_write(execute_string);

		Experiment exp;
		exp.alpha = alpha;
		exp.dataset = dataset;
		exp.featureGenerator = "baseline";
		exp.nbvars = nb_vars;
		myfile << auto_alphatest(exp, trainingData, testData, validData) << endl;


		sdd_manager_free(g_mgr);
	}
}
