/*
 * alpha_experiments.cpp
 *
 *  Created on: 7 Apr 2014
 *      Author: jessa
 */

#include "experiments.h"
#include "log.h"


const string resultfile = resultpath + "alpha.csv";

const double alpha_values[7] = {0, 0.000001, 0.00001,0.0001, 0.001, 0.01, 0.1};

string alphatest(Experiment exp, string ts, string test, string valid, bool plot);
map<string,set<double>> getCompletedExperiments(string path);
bool isCompleted(map<string,set<double>> completedExperiments, string dataset, double alpha);
void runAlphaExperiment(ofstream& myfile, map<string,set<double>> completedExperiments, string dataset, int alpha_first, int alpha_last, bool plot);


void runAlphaExperiment(){

	map<string,set<double>> completedExperiments = getCompletedExperiments(resultfile);

	ofstream myfile;
	myfile.open(resultfile,ios::app);
	if (myfile.is_open()){
		for(string dataset : datasets){
			runAlphaExperiment(myfile, completedExperiments, dataset,0,6,false);
		}

	    myfile.close();
	}

	else cout << "Unable to open file " + resultfile;
}


void runAlphaExperiment(string dataset, int alpha_first, int alpha_last, bool plot){
	string path = resultpath+"alpha_"+dataset+"_"+to_string(alpha_first)+"_"+to_string(alpha_last)+".csv";

	map<string,set<double>> completedExperiments = getCompletedExperiments(path);

	ofstream myfile;
	myfile.open(path,ios::app);
	if (myfile.is_open()){
		runAlphaExperiment(myfile,completedExperiments,dataset, alpha_first, alpha_last, plot);
	    myfile.close();
	}

	else cout << "Unable to open file " + path;
}


void runAlphaExperiment(ofstream& myfile, map<string,set<double>> completedExperiments, string dataset, int alpha_first, int alpha_last, bool plot){
	int nb_vars = getNbVars(getConfigPath(dataset));
	string trainingData = getTSPath(dataset);
	string validData    = getValidPath(dataset);
	string testData     = getTestPath(dataset);

	for(int i=alpha_first; i <=alpha_last ; i++){
		double alpha = alpha_values[i];
		if(!isCompleted(completedExperiments, dataset, alpha)){
			//			Data trainingData = Data(getTSPath(dataset),nb_vars_w,true);
			//			Data validData    = Data(getValidPath(dataset),nb_vars_w,true);
			//			Data testData     = Data(getTestPath(dataset),nb_vars_w,false);

			string execute_string = "EXECUTE: dataset: " + dataset  + ", alpha: " + to_string(alpha);
			cout << execute_string << endl;
			log_write(execute_string);

			Experiment exp;
			exp.alpha = alpha;
			exp.dataset = dataset;
			exp.featureGenerator = "baseline";
			exp.nbvars = nb_vars;
			myfile << alphatest(exp, trainingData, testData, validData, plot) << endl;


			sdd_manager_free(g_mgr);
		}
	}
}



string alphatest(Experiment exp, string ts, string test, string valid, bool plot) {
	BaselineFeatureGenerator bfg;
//	int nb_vars = exp.nbvars+1; //workaround
	int nb_vars = exp.nbvars; //workaround

	string plotfile = "plot_alphatest_" + exp.dataset + "_" + to_string(exp.alpha) + "_" + exp.featureGenerator + ".csv";

	log_indent();
	LearnedModel model = learnModel(nb_vars, ts,valid,test,&bfg, exp.alpha,true,plotfile);
	log_dindent();
	log_white_line();
	log_add_line();
	log_add_line();
	log_white_line();
//	bfg.deref(g_mgr_model); //old FG
	string res = toString(exp, model);
	return res;
}

map<string,set<double>> getCompletedExperiments(string path){
	map<string,set<double>> completedExperiments;
	ifstream file ( path ); // declare file stream: http://www.cplusplus.com/reference/iostream/ifstream/
	if (file.is_open()){
		string line;
		while (getline(file, line)) {
			Experiment exp = toExperiment(line);

			string completed = "ALREADY COMPLETED: dataset: " + exp.dataset + " alpha: " + to_string(exp.alpha);
			cout << completed << endl;
			log_write(completed);
			completedExperiments[exp.dataset].insert(exp.alpha);
		}
		file.close();
	}

	return completedExperiments;
}

 bool isCompleted(map<string,set<double>> completedExperiments, string dataset, double alpha){
	 if(completedExperiments.find(dataset)==completedExperiments.end()){
		 return false;
	 }
	 else{
		 return completedExperiments[dataset].find(alpha)!=completedExperiments[dataset].end();
	 }
 }
