/*
 * Model.h
 *
 *  Created on: 13 Apr 2014
 *      Author: jessa
 */

#ifndef MODEL_H_
#define MODEL_H_

#include <string>
#include <vector>

#include "weightLearner/weightlearner.h"

#include "../Data/Data.h"
#include "../Term/Term.h"

using namespace std;

class Model {
private:
	int m_nb_vars;
	vector<long double> m_weights;
	vector<Term> m_features;
	Term m_theory;

	Data m_trainData;
	Data m_validData;

	bool m_partition_up_to_date;
	long double m_partition;
	vector<long double> m_probabilities;

	void initialize();

	Data completeCounts(Data data);
	void checkPartition();


	/*
	 * precondition: wmc mgr should be checked, count should be complete
	 */
	long double getLogLikelihood(const Data data);

public:
	Model(int nb_vars, Data trainData, Data validData):
		m_nb_vars(nb_vars), m_partition_up_to_date(false),
		m_trainData(trainData),
		m_validData(validData){
		initialize();
	}
	~Model();

	int getNbVars() const {return m_nb_vars;}
	int getNbFeatures() const {return m_features.size()-1;}

	Data getTrainingData() const {return m_trainData;}
	Data getValidationData() const {return m_validData;}

	Model addFeature(Term feature);
	Term getMostRecentFeature() const {return m_features.back();}
	Term getFeature(int f) const {return m_features[f];}
	string getFeaturesString();

	void completeTrainCounts(){m_trainData = completeCounts(m_trainData);}
	void completeValidCounts(){m_validData = completeCounts(m_validData);}

	set<int> getDependencies(int i) const {return m_features[i].getDependencies();}

	void learnWeights();
	void printAllWeightsAndProbabilities();

	void minimize(){sdd_manager_minimize(g_mgr);}

	double getValidationLogLikelihood();
	long double calculateLogLikelihood(Data data);
	long int getSize() {return sdd_size(m_theory.toSdd());}
	long int getCount() {return sdd_count(m_theory.toSdd());}
	double getScore(double alpha, double LL) {return LL-alpha*getSize();}
	double getScore(double alpha) {return getScore(alpha, getValidationLogLikelihood());}

	void ref();
	void deref();
	void free();

	void print();

	void printTrainData() {m_trainData.print();}
	void printValidData() {m_validData.print();}

	void printTrainCount() {m_trainData.printCount();}
	void printValidCount() {m_validData.printCount();}
};

#endif /* MODEL_H_ */
