/*
 * Model.cpp
 *
 *  Created on: 13 Apr 2014
 *      Author: jessa
 */

#include "Model.h"

bool debug_m = false;


Model::~Model() {
	// TODO Auto-generated destructor stub
}

void Model::initialize(){
	// initialize features, weights and theory

	for(int f=0 ; f<=m_nb_vars ; f++){
		m_features.push_back(Term(f));
		m_weights.push_back(0);
	}
	m_theory = Term::T(); //workaround
//	m_theory = m_features[1]; //workaround
}

Model Model::addFeature(Term feature){
	if(feature.isConstant())
		return Model(*this);
	int featureNb = m_features.size();

	if(sdd_manager_var_count(g_mgr)<featureNb){
		sdd_manager_add_var_after_last(g_mgr);
	}

	Model extendedModel = Model(*this);

	extendedModel.m_partition_up_to_date=false;
	extendedModel.m_features.push_back(feature);
	extendedModel.m_weights.push_back(0);

	vector<Term> theory_elements;
	theory_elements.push_back(m_theory); // old theory
	theory_elements.push_back(Term::equiv(Term(featureNb),feature)); // new feature
//	theory_elements.push_back(Term::disjoin(m_features[1],feature,false)); // workaround;

	extendedModel.m_theory = Term::conjoin(theory_elements);
	return extendedModel;
}

Data Model::completeCounts(Data data){

	unsigned int featureToAdd = data.getNbFeatures()+1;
	while(featureToAdd<m_features.size()){
		data.addFeature(m_features[featureToAdd]);
		featureToAdd++;
	}
	if(debug_m)
		data.printCount();

	return data;
}

void Model::learnWeights(){
	completeTrainCounts();

	WeightLearner wl;
	int nbFeatures = m_trainData.getNbFeatures();
	int nbSamples  = m_trainData.getNbSamples();
	int* count = m_trainData.getCountPointer();
	long double* w = &m_weights[1];
	SddNode* sdd = m_theory.toSdd();

	if(debug_m){
			string countstr = "";
			string wstr = "";
			for(int i =0 ; i<nbFeatures;i++){
				countstr +=" "+to_string(*(count+i));
				wstr +=" "+to_string(*(w+i));
			}

			cout << "nbFeatures: " << nbFeatures << endl;
			cout << "nbSamples: "  << nbSamples  << endl;
			cout << "counts: " << countstr << endl;;
			cout << "nbSamples: " << m_trainData.getNbSamples() << endl;
			cout << "weights: " << wstr << endl;
			cout << "sdd: " << sdd_to_string(sdd) << endl;
		}

	bool print = false;
	WeightLearningProblem problem(sdd, g_mgr, nbFeatures, count, nbSamples,w,false);
	if(wl.optimize(&problem)<0){
		print = true;
		cout << "SOMETHING WENT WRONG with " << m_features.back().toString() << endl;
//		throw 3;
	}

	if(print || debug_m){
				string countstr = "";
				string wstr = "";
				for(int i =0 ; i<nbFeatures;i++){
					countstr +=" "+to_string(*(count+i));
					wstr +=" "+to_string(*(w+i));
				}

				cout << "nbFeatures: " << nbFeatures << endl;
				cout << "nbSamples: "  << nbSamples  << endl;
				cout << "counts: " << countstr << endl;;
				cout << "weights: " << wstr << endl;
				cout << "sdd: " << sdd_to_string(sdd) << endl;
			}

	m_partition_up_to_date=false;
}

void Model::printAllWeightsAndProbabilities(){
	checkPartition();
	cout <<"var - weight - logprob - prob - prop in train data - prob in valid data:" <<endl;
	for(unsigned int i=1 ; i<m_weights.size() ; i++)
		cout << i << ": " << m_weights[i]<< "  " << m_probabilities[i] << "  " << exp(m_probabilities[i]) << "  " << double(m_trainData.getCountOf(i))/m_trainData.getNbSamples() << "  " << double(m_validData.getCountOf(i))/m_validData.getNbSamples() << endl;
}


void Model::checkPartition(){
	if(!m_partition_up_to_date){
		WmcManager* wmc_mgr = wmc_manager_new(m_theory.toSdd(),1, g_mgr);
		for(unsigned int i=1 ; i<m_weights.size() ; i++){
			wmc_set_literal_weight(i, m_weights[i],wmc_mgr); //TODO
		}
		m_partition = wmc_propagate(wmc_mgr);

		m_probabilities.clear();
		m_probabilities.push_back(0);
		for(unsigned int i=1 ; i<m_weights.size() ; i++){
			m_probabilities.push_back(wmc_literal_pr(i, wmc_mgr));
		}


		wmc_manager_free(wmc_mgr);
	}
	m_partition_up_to_date = true;
}

long double Model::getLogLikelihood(const Data data){
	long double LL = 0;

	for(long int i =1 ; i<=data.getNbFeatures() ; i++){
		LL += data.getCountOf(i)*m_weights[i];
	}

	if(debug_m){
		cout << "sum = " << LL <<"  , #samples = " << data.getNbSamples() << "  , partition = " << m_partition << endl;
	}


	LL = LL/data.getNbSamples()-m_partition;

	return LL;
}

double Model::getValidationLogLikelihood(){
	completeValidCounts();
	checkPartition();
	return getLogLikelihood(m_validData);
}

long double Model::calculateLogLikelihood(Data data){
	data = completeCounts(data);
	checkPartition();
	return getLogLikelihood(data);
}

string Model::getFeaturesString(){
	string str = "[ " + getFeature(1).toString();
	for(int f=2 ; f<m_features.size() ; f++){
		str += " , " + getFeature(f).toString();
	}
	str+= " ]";
	return str;
}

void Model::ref() {
	for(Term feature : m_features){
		sdd_ref(feature.toSdd(),g_mgr);
	}
	sdd_ref(m_theory.toSdd(),g_mgr);
//	m_trainData.ref();
//	m_validData.ref();
}
void Model::deref() {
	sdd_deref(m_theory.toSdd(),g_mgr);
	for(Term feature : m_features){
		sdd_deref(feature.toSdd(),g_mgr);
	}
//	m_trainData.deref();
//	m_validData.deref();
}


void Model::free(){
	deref();
}

void Model::print(){
	cout << "PRINT MODEL" << endl <<endl;
	cout << "  nb vars = " << m_nb_vars << endl <<endl;
	cout << "  features: " << endl;
	for (unsigned int i=1 ; i<m_features.size() ; i++){
		cout << "  " << i <<": " << m_features[i].toString() << endl;
	}
	cout << endl;
	cout << "  weights: " << endl;
	for (unsigned int i=1 ; i<m_features.size() ; i++){
		cout << "  " << i <<": " << m_weights[i] << endl;
	}
	cout << endl;
	cout << "  theory: " << m_theory.toString() << endl << endl;
	cout << "  sdd: " << sdd_to_string(m_theory.toSdd()) << endl << endl;
}
