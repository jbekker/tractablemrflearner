#ifndef WEIGHTLEARNER_H_JUL_27_2013
#define WEIGHTLEARNER_H_JUL_27_2013

#include <stdio.h>
#include <iostream>
#include <ctime>
#include <cmath> // required for infinite stuff to work?
#include "lbfgs/lbfgs.h"
#include <limits>
//#include <sddapi.h>

/*EDIT DOOR JESSA */
#include "sdd_interface.h"

using namespace std;
/**
 * Class representing a (maximum likelihood) weight learning problem.
 */

class LikelihoodInferenceProblem{

public:
  SddNode* sdd;
  SddManager* mgr;
  int n; // num_features
  int* counts;
  int nbInstances;
  long double* weights;
  bool debug;

  WmcManager* wmcManager;

//  LikelihoodInferenceProblem(SddNode* sdd, SddManager* mgr,
//        int n, int* counts, int nbInstances, long double* weights,
//        bool debug, int *h_, int *g_)
//    : sdd(sdd), mgr(mgr), n(n), counts(counts), nbInstances(nbInstances), weights(weights), debug(debug),
//      h(h_), g(g_)
  LikelihoodInferenceProblem(SddNode* sdd, SddManager* mgr,
        int n, int* counts, int nbInstances, long double* weights,
        bool debug)
    : sdd(sdd), mgr(mgr), n(n), counts(counts), nbInstances(nbInstances), weights(weights), debug(debug)
  {
//    wmcManager = new_wmc_manager(sdd, 1, mgr);
	  wmcManager = wmc_manager_new(sdd, 1, mgr);
    // negative weights are 1;
    for(int i=1;i<=sdd_manager_var_count(mgr);i++){
    	wmc_set_literal_weight(-i,0,wmcManager);
//      set_literal_weight(-i,0,wmcManager);
    }
  }

  virtual ~LikelihoodInferenceProblem(){
//    if(wmcManager != NULL) free_wmc_manager(wmcManager);
	  if(wmcManager !=NULL){
		  wmc_manager_free(wmcManager);
	  }

  }

  lbfgsfloatval_t get_weight(const lbfgsfloatval_t* cur_weights, int idx) const{
//      if (idx==-1)
//        return 0;
//      else
//      if (idx==0)
//        return -1e100;
//      else
        return cur_weights[idx-1];
  }

  // calculate likelihood
  long double infer(const lbfgsfloatval_t* cur_weights){
    // set weights in wmcManager
    if(debug){
      cout << "Setting weights to [";
      for(int i = 1; i <= sdd_manager_var_count(mgr); i++) {
        cout << get_weight(cur_weights, i) << ", ";
      }
      cout << "]" << endl;
    }
    /* EDIT DOOR JESSA */
    if(debug){
    	for (int i=1 ; i<=sdd_manager_var_count(mgr);i++){
    		cout << i << ": " << wmc_literal_weight(i,wmcManager) << " +++ " << wmc_literal_weight(i,wmcManager) << endl;
    	}
    	cout << "LogWMCInit: " <<wmc_propagate(wmcManager)<<endl;
    }


    for(int i=1;i<=sdd_manager_var_count(mgr);i++){
//      set_literal_weight(i,get_weight(cur_weights, i),wmcManager);
    	wmc_set_literal_weight(i,get_weight(cur_weights, i), wmcManager);
    }

    // compute WMC and gradiřent
//    SddWmc logWmc = sdd_weighted_model_count(wmcManager);
    SddWmc logWmc = wmc_propagate(wmcManager);
    if(debug) cerr << "LogWMC = " << logWmc << endl;
    assert(std::isfinite(logWmc));

    long double negll = nbInstances*logWmc;
    //int tot = 0;
    for (int i = 0; i < n; i++) {
      //totř += counts[i];
      negll -= (counts[i] * cur_weights[i]);
    }
    //assert(tot==nbInstances);
    if(debug) cout << "eval ll: " << (-negll/nbInstances) << endl;
    assert(negll >= -1e-6);
    assert(std::isfinite(negll));
    return negll;
  }

  long double inferPerInstance(const lbfgsfloatval_t* cur_weights){
    return -infer(cur_weights)/nbInstances;
  }

  long double inferPerInstance(){
    return inferPerInstance(weights);
  }

};


/**
 * Class representing a (maximum likelihood) weight learning problem.
 */
class WeightLearningProblem: public LikelihoodInferenceProblem{

public:

  // per instance log likelihood (result)
  long double loglikelihood;

  int cnt_wmc;
  double time_wmc1, time_wmc2;

//  WeightLearningProblem(SddNode* sdd, SddManager* mgr,
//      int nbFeatures, int* counts, int nbInstances, long double* weights,
//      bool debug, int *h_, int *g_)
//  : LikelihoodInferenceProblem(sdd,mgr,nbFeatures,counts,nbInstances,weights,debug, h_, g_),
  WeightLearningProblem(SddNode* sdd, SddManager* mgr,
      int nbFeatures, int* counts, int nbInstances, long double* weights,
      bool debug)
  : LikelihoodInferenceProblem(sdd,mgr,nbFeatures,counts,nbInstances,weights,debug),
    cnt_wmc(0), time_wmc1(0), time_wmc2(0)
  {
    loglikelihood = std::numeric_limits<long double >::infinity();
  }

  // calculate likelihood and gradient
  lbfgsfloatval_t evaluate(
      const lbfgsfloatval_t* cur_weights,
      lbfgsfloatval_t* gradient,
      const int n,
      const lbfgsfloatval_t step
      ){
    clock_t timer1 = clock();
    // get likelihood from super class
    long double negll = infer(cur_weights);
    clock_t timer2 = clock();
    // calculate gradient
    for (int i = 0; i < n; i++) {
//        long double marginalProb = logProb2Prob(literal_pr(h[i], wmcManager));
        long double marginalProb = logProb2Prob(wmc_literal_pr(i+1, wmcManager));
      assert(std::isfinite(marginalProb));
      assert(marginalProb<=1+1e-6);
      assert(marginalProb>=-1e-6);
//      if (i!=-3)
        gradient[i] = -(counts[i] - marginalProb*nbInstances);
//      else
//        gradient[i] = 0;
      if(debug){
        cout << "P_m("<<i<<") = " << marginalProb << endl;
        cout << "P_d("<<i<<") = " << (counts[i]*1.0/nbInstances) << endl;
        cout << "gradient[" << i << "] = " << gradient[i] << endl;
      }
      assert(std::isfinite(gradient[i]));
    }
    clock_t timer3 = clock();
    time_wmc1 += (timer2-timer1)*1.0/CLOCKS_PER_SEC;
    time_wmc2 += (timer3-timer2)*1.0/CLOCKS_PER_SEC;

    cnt_wmc += 1;
    return negll;
  }

  void done(lbfgsfloatval_t *m_x, lbfgsfloatval_t fx){
    for(int i=0;i<n;i++) {
      weights[i] = m_x[i];
    }
    loglikelihood = -fx/nbInstances;
  }

private:

  static inline long double logProb2Prob(long double logProb){
    if(logProb>0 && logProb < 0.0000000000001) return 1;
    else return expl(logProb);
  }

};

static const long double PI  = 3.141592653589793238463;
/**
 * Class that takes weight learning problems and solves them.
 */
class WeightLearner
{

protected:

  static const long double priorMean;
  long double priorSigma;

  lbfgs_parameter_t settings;

public:

    int numberOfEvaluations;

    WeightLearner(long double priorSigma = 2, long double l1Const = 0.95,
        int maxIter = 70, long double delta = 1e-10, long double epsilon = 1e-4): priorSigma(priorSigma) {
      lbfgs_parameter_init(&settings);
      if(l1Const!=0) settings.orthantwise_c = l1Const;
      settings.max_iterations = maxIter;
      settings.delta = delta;
      settings.epsilon = epsilon; // to avoid going to nan?
      numberOfEvaluations = 0;
    }

    struct LearningRun{
      WeightLearningProblem* problem;
      WeightLearner* learner;
    } ;

    int optimize(WeightLearningProblem* problem)
    {

    	/* EDIT DOOR JESSA */

    	if(problem->debug){
    		cout << endl;
    		cout << "sdd manager:" << endl << sdd_to_string(problem->mgr) << endl;
    		cout << "sdd manager statistics:";
    		sdd_manager_print(problem->mgr);
    		cout << "nbInstances: " << problem->nbInstances << endl;
    		cout << "nbFeatures: "  << problem->n << endl;
    		cout << "sdd: "         << sdd_to_string(problem->sdd) << endl;
    		cout << "counts: ";
    		for(int i=0 ; i<problem->n ;i++)
    			cout << problem->counts[i] << " ";
    		cout << endl <<"weights: ";
    		for(int i=0 ; i<problem->n ;i++)
    			cout << problem->weights[i] << " ";
    		cout << endl << endl;
    	}



        /* Initialize the variables and settings. */
        lbfgsfloatval_t fx;
        lbfgsfloatval_t *m_x = lbfgs_malloc(problem->n);
        if (m_x == NULL) {
            printf("ERROR: Failed to allocate a memory block for variables.\n");
            return 1;
        }
        for (int i = 0;i < problem->n;i++) {
            m_x[i] = problem->weights[i];
        }
        if(settings.orthantwise_c!=0) {
          settings.linesearch = LBFGS_LINESEARCH_BACKTRACKING; //required by lbfgs lib
          settings.orthantwise_start = 0; //problem->nbUnitFeatures;
          settings.orthantwise_end = problem->n; // incorporate all parameters for non-unit features in L1 norm
        }
        numberOfEvaluations = 0;

        /*
            Start the L-BFGS optimization; this will invoke the callback functions
            evaluate() and progress() when necessary.
         */
        LearningRun run;
        run.problem = problem;
        run.learner = this;
        int ret = lbfgs(problem->n, m_x, &fx, _evaluate, _progress, &run, &settings);
        if(problem->debug || ret < 0) {
          cout << "L-BFGS optimization terminated with status code = " << ret << ", fx = " << fx << endl;
        }
        problem->done(m_x,fx);
        lbfgs_free(m_x);
        return ret;
    }

    /**
     * tune parameter of this learner, also updating the trained weights with the new parameters
     */
    long double tuneParameter(WeightLearningProblem* trainedWeights, LikelihoodInferenceProblem* tuningProblem,
        long double &parameter, long double step = 0.9, string name = "parameter"){

      // backup original values
      long double parameterBefore = parameter;
      long double weightsBefore[trainedWeights->n];
      for (int jx = 0; jx < trainedWeights->n; jx++) {
        weightsBefore[jx] = trainedWeights->weights[jx];
      }
      long double trainingLoglikelihoodBefore = trainedWeights->loglikelihood;

      // compute initial tuning set score
      long double tuningLogLikelihoodBefore = tuningProblem->inferPerInstance();

      // try parameter/step, relearn and reevaluate
      parameter = parameterBefore/step;
      if(optimize(trainedWeights) < 0){
        cerr << "ERROR: Failed to optimize weights while tuning, cannot proceed." << endl;
        exit(-1);
      }
      long double tuningLoglikelihood = tuningProblem->inferPerInstance(trainedWeights->weights);

      // check for improvement
      if(tuningLoglikelihood > tuningLogLikelihoodBefore){
        cout << "Increasing " << name << " from " << parameterBefore << " to " << parameter
            << "(" << tuningLoglikelihood << " > " << tuningLogLikelihoodBefore << ")"  << endl;
        return tuningLoglikelihood;
      }

      // no improvement, reset weights
      for (int jx = 0; jx < trainedWeights->n; jx++) {
        trainedWeights->weights[jx] = weightsBefore[jx];
      }
      trainedWeights->loglikelihood = trainingLoglikelihoodBefore;

      // try parameter*step, relearn and reevaluate
      parameter = parameterBefore*step;
      if(optimize(trainedWeights) < 0){
        cerr << "ERROR: Failed to optimize weights while tuning, cannot proceed." << endl;
        exit(-1);
      }
      tuningLoglikelihood = tuningProblem->inferPerInstance(trainedWeights->weights);

      // check for improvement
      if(tuningLoglikelihood > tuningLogLikelihoodBefore){
        cout << "Decreasing " << name << " from " << parameterBefore << " to " << parameter
            << "(" << tuningLoglikelihood << " > " << tuningLogLikelihoodBefore << ")"  << endl;
        return tuningLoglikelihood;
      }

      // no improvement, reset weights
      parameter = parameterBefore;
      for (int jx = 0; jx < trainedWeights->n; jx++) {
        trainedWeights->weights[jx] = weightsBefore[jx];
      }
      trainedWeights->loglikelihood = trainingLoglikelihoodBefore;

      cout << "Keeping " << name << " at " << parameter << "(" << tuningLogLikelihoodBefore << ")"  << endl;

      return tuningLogLikelihoodBefore;
    }


    long double tunePriorSigma(WeightLearningProblem* trainedWeights, LikelihoodInferenceProblem* tuningProblem, long double step = 0.9){
      return tuneParameter(trainedWeights,tuningProblem,priorSigma,step,"prior sigma");
    }

    long double tunel1Const(WeightLearningProblem* trainedWeights, LikelihoodInferenceProblem* tuningProblem, long double step = 0.9){
      return tuneParameter(trainedWeights,tuningProblem,settings.orthantwise_c,step,"l1 constant");
    }

    long double tuneParameters(WeightLearningProblem* trainedWeights, LikelihoodInferenceProblem* tuningProblem, long double step = 0.9){
//      if(priorSigma==0 && settings.orthantwise_c!=0){
//        return tunel1Const(trainedWeights,tuningProblem,step);
//      }
//      if(settings.orthantwise_c==0 && priorSigma!=0){
//        return tunePriorSigma(trainedWeights,tuningProblem,step);
//      }
//      if(priorSigma!=0 && settings.orthantwise_c!=0){
//        tunel1Const(trainedWeights,tuningProblem,step);
//        return tunePriorSigma(trainedWeights,tuningProblem,step);
//      }
      cout << "No parameters to tune."  << endl;
      return tuningProblem->inferPerInstance();
    }

    void copyTunedParameters(WeightLearner* learner){
      priorSigma = learner->priorSigma;
      settings.orthantwise_c = learner->settings.orthantwise_c;
    }

protected:

    static lbfgsfloatval_t _evaluate(
        void *instance,
        const lbfgsfloatval_t* cur_weights,
        lbfgsfloatval_t *gradient,
        const int n,
        const lbfgsfloatval_t step){
      LearningRun* run = reinterpret_cast<LearningRun*>(instance);
      lbfgsfloatval_t negll =  run->problem->evaluate(cur_weights, gradient, n, step);
      // in addition to the L1 regularization, add the gaussian priors
      return run->learner->addPrior(run->problem,negll,cur_weights, gradient, n, step);
    }



    lbfgsfloatval_t addPrior(
            WeightLearningProblem* problem,
            lbfgsfloatval_t negll,
            const lbfgsfloatval_t* cur_weights,
            lbfgsfloatval_t *gradient,
            const int n,
            const lbfgsfloatval_t step){
      if(priorSigma>0){
          for (int i = 0; i < n; i++) {
            negll -= -(cur_weights[i] - priorMean) * (cur_weights[i] - priorMean) / (2.0 * priorSigma * priorSigma) ;
            negll -= -log(priorSigma) - log(2*PI)/2.0 ; //needed to compare different sigmas in parameter tuning, is a constant when comparing different wiehgts
            gradient[i] -= (priorMean - cur_weights[i]) / (priorSigma * priorSigma );
            if(problem->debug){
              cout << "gaussian (sigma " << priorSigma << ") regularized gradient[" << i << "] += "
                  << ((cur_weights[i] - priorMean) / (priorSigma * priorSigma )) << " = " << gradient[i] << endl;
            }
            assert(std::isfinite(gradient[i]));
          }
          if(problem->debug){
            cout << "gaussian regularized negll = " << negll << endl;
          }
          assert(std::isfinite(negll));
      }
      return negll;
    }

    static int _progress(
        void *instance,
        const lbfgsfloatval_t *x,
        const lbfgsfloatval_t *g,
        const lbfgsfloatval_t fx,
        const lbfgsfloatval_t xnorm,
        const lbfgsfloatval_t gnorm,
        const lbfgsfloatval_t step,
        int n,
        int k,
        int ls
        )
    {
        LearningRun* run = reinterpret_cast<LearningRun*>(instance);
        return run->learner->progress(run->problem, x, g, fx, xnorm, gnorm, step, n, k, ls);
    }

    // report progress per iteration
    int progress(
        WeightLearningProblem* problem,
        const lbfgsfloatval_t *x,
        const lbfgsfloatval_t *g,
        const lbfgsfloatval_t fx,
        const lbfgsfloatval_t xnorm,
        const lbfgsfloatval_t gnorm,
        const lbfgsfloatval_t step,
        int n,
        int k,
        int ls
        )
    {
        numberOfEvaluations += ls;
        if(problem->debug){
          printf("Iteration %d:\n", k);
          printf("  fx = %Lf, ", fx);
          for(int i=0;i<n;i++){
            printf("  x[%d] = %Lf,", i, x[i]);
          }
          printf("\n");
          printf("  xnorm = %Lf, gnorm = %Lf, step = %Lf\n", xnorm, gnorm, step);
          printf("\n");
        }
        return 0;
    }
};

#endif
