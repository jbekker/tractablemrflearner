/*
 * sdd_interface.cpp
 *
 *  Created on: 13 Apr 2014
 *      Author: jessa
 */

#include "sdd_interface.h"

int g_auto_gb = 0;
SddManager* g_mgr = sdd_manager_create(1,g_auto_gb);
SddManager* g_mgr_data = sdd_manager_create(1,g_auto_gb);


SddNode* sdd_imply(SddNode* n1, SddNode* n2, SddManager* mgr){
	sdd_ref(n2,mgr);
	sdd_ref(n1,mgr);

	SddNode* result = sdd_disjoin(sdd_negate(n1,mgr),n2,mgr);

	sdd_deref(n1,mgr);
	sdd_deref(n2,mgr);

	return result;
}

SddNode* sdd_equiv(SddNode* n1, SddNode* n2, SddManager* mgr){
	sdd_ref(n1,mgr);
	sdd_ref(n2,mgr);

	SddNode* lhs = sdd_imply(n1,n2,mgr);
	sdd_ref(lhs,mgr);
	SddNode* rhs = sdd_imply(n2,n1,mgr);
	SddNode* result =  sdd_conjoin(lhs,rhs,mgr);
	sdd_deref(lhs,mgr);

	sdd_deref(n1,mgr);
	sdd_deref(n2,mgr);

	return result;
}

SddNode* sdd_xor(SddNode* n1, SddNode* n2,SddManager* mgr){
	sdd_ref(n1,mgr);
	sdd_ref(n2,mgr);

	SddNode* lhs = sdd_conjoin(sdd_negate(n1,mgr),n2,mgr);
	sdd_ref(lhs,mgr);
	SddNode* result =  sdd_disjoin(lhs,sdd_conjoin(n1,sdd_negate(n2,mgr),mgr),mgr);
	sdd_deref(n1,mgr);

	sdd_deref(n2,mgr);
	sdd_deref(lhs,mgr);
	return result;
}

string sdd_to_string(SddNode* sdd){
	if(sdd_node_is_decision(sdd)){
		long size = sdd_node_size(sdd);
		SddNode** sdds = sdd_node_elements(sdd);
		string str = "(+";
		for(long i=0; i<size;i++){
			string prime = sdd_to_string(*sdds);
			sdds++;
			string sub = sdd_to_string(*sdds);
			str+="("+ prime+"=>" + sub +")";
			sdds++;
		}
		str+=")";
		return str;
	}
	else if(sdd_node_is_literal(sdd)){
		return to_string(sdd_node_literal(sdd));
	}
	else if(sdd_node_is_true(sdd)){
		return "T";
	}
	return "F";
}

string sdd_to_string(Vtree* vtree){
	string str = "( " + to_string(sdd_vtree_position(vtree));
	if(sdd_vtree_is_leaf(vtree)){
		str += " " + to_string(sdd_vtree_var(vtree));
	}
	else{
		str += " " + sdd_to_string(sdd_vtree_left(vtree)) + " " + sdd_to_string(sdd_vtree_right(vtree));
	}
	str+=")";
	return str;
}

string sdd_to_string(SddManager* mgr){
	string str = "var count: " + to_string(sdd_manager_var_count(mgr)) + "\n";
	str += "size (total, live, dead): " + to_string(sdd_manager_size(mgr)) +" , " + to_string(sdd_manager_live_size(mgr)) +" , " + to_string(sdd_manager_dead_size(mgr)) + "\n";
	str += "count (total, live, dead): " + to_string(sdd_manager_count(mgr)) +" , " + to_string(sdd_manager_live_count(mgr)) +" , " + to_string(sdd_manager_dead_count(mgr)) + "\n";
//	str += "vtree: " + sdd_to_string(sdd_manager_vtree(mgr));


	return str;
}



