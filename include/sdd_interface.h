/*
 * sdd_interface.h
 *
 *  Created on: 13 Apr 2014
 *      Author: jessa
 */

#ifndef SDD_INTERFACE_H_
#define SDD_INTERFACE_H_


extern "C"{
	#include "sddapi.h"
}

#include <string>
#include "iostream"


using namespace std;

extern int g_auto_gb;
extern SddManager* g_mgr;

SddNode* sdd_imply(SddNode* n1, SddNode* n2, SddManager* mgr);
SddNode* sdd_equiv(SddNode* n1, SddNode* n2, SddManager* mgr);
SddNode* sdd_xor(SddNode* n1, SddNode* n2,SddManager* mgr);

string sdd_to_string(SddNode* sdd);
string sdd_to_string(Vtree* vtree);
string sdd_to_string(SddManager* mgr);


#endif /* SDD_INTERFACE_H_ */
