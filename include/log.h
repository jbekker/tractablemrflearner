/*
 * log.h
 *
 *  Created on: 16 Apr 2014
 *      Author: jessa
 */

#ifndef LOG_H_
#define LOG_H_

#include <string>
#include <fstream>

extern std::string g_log_file_path;

extern int g_log_cur_indent_length;
extern std::string g_log_cur_indent;

void log_write( const std::string &text );

void log_white_line();

void log_set_path(std::string path);

void log_add_line();

void log_indent();
void log_dindent();


#endif /* LOG_H_ */
